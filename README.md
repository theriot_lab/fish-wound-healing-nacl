# Electrical cues guide wound healing in zebrafish epidermis

Code related to understanding the mechanisms of wound detection in zebrafish epidermis.

Preprint to cite: Kennard AS, Theriot JA. 2020. Osmolarity-independent electrical cues guide rapid response to injury in zebrafish epidermis. bioRxiv doi:[https://doi.org/10.1101/2020.08.05.237792](https://doi.org/10.1101/2020.08.05.237792)

## Repo organization

Currently, most available code and data is for re-plotting or re-analyzing the figures in the text (all MATLAB-based).

The scripts to plot each figure are in the `plot_figures` folder; you can run an entire script and produce each panel, or you can run an individual section to look at a single panel. Most of the parameters that encode choices about data analysis or figure display are encoded in functions in the `config` subfolder. I tried to organize it so that each function outputs a struct of `dataProcessingDetails` and a struct of `plotDetails` for each figure panel, though you may disagree with the semantics of how I split parameters up between those two structs. 

All the data is located in the `data` folder, and I may add some more description about the organization of those individual data files, but it is a lot of .mat and .csv files. 

I included useful functions in `src`, including functions from MathWorks FileExchange that made my life a lot easier. Thank you to those who uploaded those functions!

### Image Registration Scripts

I have also added Python code I used for motion correction and image registration in the `python_feature_registration` folder located in `src`. More details on the operation of that code is available in the README of that folder.

## Notes for running the code

1. Clone the repository to download all the code to your local machine.
    - If you're not sure how to do this, instructions for how to clone a repository from Github should work just as well for Gitlab.
2. Make sure to [add the folder and all subfolders](https://www.mathworks.com/matlabcentral/answers/116177-how-to-add-a-folder-permanently-to-matlab-path#answer_381809) to your MATLAB path.
3. Make sure you have [BioFormats installed](https://docs.openmicroscopy.org/bio-formats/6.1.0/users/matlab/index.html) for MATLAB. I use BioFormats for opening and reading files. I think that is the only major dependency (I don't think you need any specific MATLAB Toolboxes to run this code), but I might be wrong about that--please let me know if that seems to be the case! 
4. While not strictly necessary, you will probably want to start by opening and running one of the scripts in `plot_figures`.

This was all written (and seems to work) in MATLAB 2018b, again with BioFormats but (probably) no other toolboxes.
