The data required to generate the figures in the paper is shared here. A dependency graph for the different datasets and the metadata describing each of the datasets is shown below:

![](DataDependencyGraph.svg)
