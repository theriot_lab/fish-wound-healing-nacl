function fig = initializeFigure(panelName, figLocation, plotDetails)
%%% Wrapper to initialize a figure based on settings in plotDetails.
%%% Also put the panel of the figure in the title of the figure and
%%% position the figure on the screen (the width and the height of the
%%% figure are set by plotDetails).
w = plotDetails.figureSize.width;
h = plotDetails.figureSize.height;
figureName = sprintf('Panel %s', panelName);
fig = figure('Position', [figLocation,w,h], 'Name',figureName);
end