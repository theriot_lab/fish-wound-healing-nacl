function dataToPlot = processDataForSpeedOverTime(dataProcessingDetails,...
                                              plotDetails, varargin)
%%% Generate a plot of speed over time, with bootstrapped 95% confidence
%%% intervals for error bars, for a set of conditions.
%Get data from the 'raw' source kymographs and turn into speed over time,
%each row is an observation, columns are time post-wounding
%Input:
%  - dataProcessingDetails: struct of information about how to process
%  details from speed kymographs
%  - plotDetails: information about what conditions to plot
%  - measureRelative (optional): true/false whether speed should be
%  measured in the "nearby" areas relative to the "far-away" areas, i.e. if
%  the "far-away" areas should be subtracted from the "nearby" areas.
%  (default false).
%Output:
% - An array of structs to facilitate plotting a set of XY graphs with
% nonoverlapping confidence intervals. Each struct in the array contains
% the following fields:
%    - condition: string that matches the condition in the figure
%    parameters
%    - meanY: the average value of the quantity plotted on the vertial axis
%    - X: the data to be plotted on the horizontal axis
%    - XforCI: the subset of positions to plot confidence intervals on the
%    horizontal axis
%    - YforCI: the subset of positions to plot confidence intervals on the
%    vertical axis (interpolated between known data points)
%    - displayCI: the confidence intervals to display.
%Note:
% - X: horizontal axis (time in this case)
% - Y: vertical axis (speed in this case)

%Parse arguments
p = inputParser;
addRequired(p, 'dataProcessingDetails', @isstruct);
addRequired(p, 'plotDetails', @isstruct);
addParameter(p, 'measureRelative', false);
parse(p, dataProcessingDetails, plotDetails, varargin{:});
dataProcessingDetails = p.Results.dataProcessingDetails;
plotDetails = p.Results.plotDetails;
measureRelative = p.Results.measureRelative;

load(dataProcessingDetails.dataFile, 'allKymographs', 'kymographTCoordinates');
metadata = readtable(dataProcessingDetails.metadataFile);
speed = computeSpeedOverTimeForKymographList(allKymographs,...
          dataProcessingDetails.rowsToAvg, 'measureRelative', measureRelative);
dataToPlot = struct('condition', [], 'meanY', [], 'X', [], 'XForCI', [],...
                    'YForCI', [], 'displayCI', []);
% Data processing: get the lines and confidence intervals to be plotted
for k = 1:plotDetails.nConditions
    %select which rows to use
    idxToUse = strcmp(metadata.experimentalTreatmentPreWounding,...
                      plotDetails.conditions(k).label);
    %compute speed over time, averaged over some amount ofthe tissue
    thisConditionSpeed = speed(idxToUse, 1:dataProcessingDetails.timepointsToShow);
    meanSpeed = nanmean(thisConditionSpeed, 1);
    
    %assume all data has the same spacing between timepoints
    dt = kymographTCoordinates{1}(2)- kymographTCoordinates{1}(1);
    t = dt * (0:(dataProcessingDetails.timepointsToShow-1));
    
    %Compute the error bars with bootstrapping
    ciSpeed = bootci(10000,@nanmean,thisConditionSpeed);
    %Choose subset to error bars to plot
    [tForCI,speedForCI,displayCI] = choosePositionsForCI(t, meanSpeed,...
                                    ciSpeed, k, plotDetails.nConditions);
    dataToPlot(k).condition = plotDetails.conditions(k).label;
    dataToPlot(k).meanY = meanSpeed;
    dataToPlot(k).X = t;
    dataToPlot(k).XForCI = tForCI;
    dataToPlot(k).YForCI = speedForCI;
    dataToPlot(k).displayCI = displayCI;
    
end