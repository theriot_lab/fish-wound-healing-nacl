import csv
import numpy as np
import argparse
import os
import warnings
import pims
from tifffile import imsave

from skimage.transform import (warp,EuclideanTransform)
from skimage.measure import ransac
from skimage import (img_as_ubyte, img_as_uint)
from skimage import io
from skimage.exposure import rescale_intensity

import cv2

def parse_data_info():
    parser = argparse.ArgumentParser()
    parser.add_argument('csv_file', help='path to csv file with information')
    parser.add_argument('-s','--short',help='use calculated transforms if'
                                             ' they exist')
    args = parser.parse_args()
    data_info = []
    with open(args.csv_file,'r',encoding='utf-8-sig') as csv_file:
        reader = csv.DictReader(csv_file,delimiter=',')
        for row in reader:
            info = initialize_from_info(row)
            data_info.append(info)
    return data_info,args

def extract_relevant_file_parts(file_string):
    dir_name, base_name = os.path.split(file_string)
    root_name,ext = os.path.splitext(base_name)
    return dir_name,base_name,root_name

def setup_save_folders(file_string,output_suffix):
    dir_name,base_name,root_name = extract_relevant_file_parts(file_string)
    save_file_root_name = '{}_{}'.format(root_name,output_suffix)
    save_folder = os.path.join(dir_name,save_file_root_name)
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder)
    return save_folder,save_file_root_name

def initialize_from_info(dataset):
    offset = (int(dataset['yPosition']), int(dataset['xPosition']))
    subimg_save_folder, subimg_root_name = setup_save_folders(
                                             dataset['subImageToRegister'],
                                             dataset['outputSuffix'])
    fullimg_save_folder, fullimg_root_name  = setup_save_folders(
                                             dataset['fullImageToRegister'],
                                             dataset['outputSuffix'])
    info = dict()
    info['offset'] = offset
    info['subimg_save_folder'] = subimg_save_folder
    info['subimg_root_name'] = subimg_root_name
    info['fullimg_save_folder'] = fullimg_save_folder
    info['fullimg_root_name'] = fullimg_root_name
    info['subImageToRegister'] = dataset['subImageToRegister']
    info['fullImageToRegister'] = dataset['fullImageToRegister']
    info['label'] = dataset['label']
    info['firstFrame'] = int(dataset['firstFrame'])
    info['lastFrame'] = int(dataset['lastFrame'])
    return info

def register_frames(keypoints0,descriptors0,keypoints1,descriptors1):
    bf_matcher = cv2.BFMatcher(crossCheck=True)
    matches = bf_matcher.match(descriptors0,descriptors1)
    #queryIdx -> first entry, trainIdx -> second entry to match
    keypoints0_matched = np.array([keypoints0[x.queryIdx].pt for x in matches])
    keypoints1_matched = np.array([keypoints1[x.trainIdx].pt for x in matches])
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        model,inliers = ransac((keypoints0_matched, keypoints1_matched),
                           EuclideanTransform,min_samples=2,
                           residual_threshold=5,max_trials=200)
    return model,inliers

def find_normalization(I):
    '''Go through the image stack and get the maximal value that will saturate 
       0.03% of pixels. Also find the minimum of the stack'''
    min_val = 65355;
    max_val = 0;
    for im in I:
        max_val = max(np.quantile(im,.997),max_val)
        min_val = min(np.min(im),min_val)
    return (min_val,max_val)

def compute_registration_transforms(info):
    I = pims.open(info['subImageToRegister'])
    if info['firstFrame']>=0:
        firstFrame = info['firstFrame']
    else:
        firstFrame = 0
    if info['lastFrame']>=0:
        im_iterator = I[(firstFrame+1):info['lastFrame']]
    else:
        im_iterator = I[(firstFrame+1):]
    in_range = find_normalization(im_iterator)
    I0 = I[firstFrame]
    I0 = rescale_intensity(I0,in_range=in_range)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        I0 = img_as_ubyte(I0)
    frame_tform_list = np.eye(3).reshape((3,3,1))
    cumul_tform_list = np.eye(3).reshape((3,3,1))
    kaze = cv2.KAZE_create()
    keypoints0,descriptors0 = kaze.detectAndCompute(I0,None)
    registered_fname = os.path.join(info['subimg_save_folder'],
                        '{0}_{1:03d}.tif'.format(info['subimg_root_name'],
                         firstFrame))
    imsave(registered_fname,I0)
    
    for k,im in enumerate(im_iterator):
        I1 = im
        I1 = rescale_intensity(I1,in_range=in_range)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            I1 = img_as_ubyte(I1)
        keypoints1,descriptors1 = kaze.detectAndCompute(I1,None)
        model,inliers = register_frames(keypoints0, descriptors0,
                                        keypoints1, descriptors1)
        cumul_tform = np.dot(cumul_tform_list[:,:,-1], model.params)
        cumul_tform_list = np.dstack((cumul_tform_list,cumul_tform))
        frame_tform_list = np.dstack((frame_tform_list,model.params))
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            I1_reg = img_as_ubyte(warp(I1,cumul_tform))
        registered_fname = os.path.join(info['subimg_save_folder'],
                            '{0}_{1:03d}.tif'.format(info['subimg_root_name'],
                                                     k+firstFrame+1))
        imsave(registered_fname,I1_reg)
        I0 = np.copy(I1)
        keypoints0 = np.copy(keypoints1)
        descriptors0 = np.copy(descriptors1)
    tform_savename = os.path.join(info['subimg_save_folder'],'tforms')
    np.savez(tform_savename,cumul_tform_list=cumul_tform_list,
              frame_tform_list=frame_tform_list)
    return cumul_tform_list

def apply_tform_to_full_frame(im,original_tform,offset):
    Y,X = offset
    a = original_tform[0,0]
    b = original_tform[1,0]
    old_tx = original_tform[0,2]
    old_ty = original_tform[1,2]
    new_tx = (1-a) * X + b * Y + old_tx
    new_ty = -b * X + (1-a) * Y + old_ty
    new_tform = np.array([[a, -b, new_tx],
                          [b,  a, new_ty],
                          [0,  0,      1]])
    im_reg = warp(im,new_tform)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        im_reg = img_as_uint(im_reg)
    return im_reg

def align_full_frames(info,tforms):
    I = pims.open(info['fullImageToRegister'])
    if info['firstFrame']>=0:
        firstFrame = 2*info['firstFrame']
    else:
        firstFrame = 0
    if info['lastFrame']>=0:
        im_iterator = I[firstFrame:(2*info['lastFrame'])]
    else:
        im_iterator = I[firstFrame:]
    for k,im in enumerate(im_iterator):
        #all are two-channel, so just divide the channels
        idx = int(k/2)
        im_reg = apply_tform_to_full_frame(im,tforms[:,:,idx],
                                           info['offset'])
        registered_fname = os.path.join(info['fullimg_save_folder'],
                            '{0}_{1:03d}.tif'.format(info['fullimg_root_name'],
                                                     k+firstFrame))
        imsave(registered_fname,im_reg)

        

def main():
    data_info,args = parse_data_info();
    for info in data_info:
        print('Processing {}...'.format(info['label']))
        if args.short:
            try:
                tforms = np.load(os.path.join(info['subimg_save_folder'],
                                   'tforms.npz'))
                print("Using short option...")
                cumul_tform_list = tforms['cumul_tform_list']
            except Exception as e:
                print(e)
                cumul_tform_list = compute_registration_transforms(info)
        else:
            print("Computing registrations...")
            cumul_tform_list = compute_registration_transforms(info)
        print("aligning full frames...")
        align_full_frames(info,cumul_tform_list)
        print("done!")
    print("All done!")


if __name__ == '__main__':
    main()
