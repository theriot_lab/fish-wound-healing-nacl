# Code for motion correction

In order to get accurate measurements of movement in the tailfin, it's important to correct for any drift of the tailfin that is not due to cell migration. 
I wrote a python script to do this. It computes the registration transformations on a sub-image (of a potentially different channel) and then applies them to the full-size
target image. 

## Installation

The required packages that you need to install are listed in `20190310_python_requirements.txt`. You should be able to install all of these
at the command line with 

    > pip install -r 20190310_python_requirements.txt
    
Once you have installed the required dependencies, I think that you can install this with `pip install python_feature_registration` at the command line.

## Usage

The basic syntax is 

    > python register_full_pipeline.py csv_file [-s]

The required `csv_file` contains information about the datasets to be registered--information about the headers in this CSV file is described below.

The optional `-s` flag will shorten the run time by looking for any pre-computed transformation files and using those instead; otherwise it will default to re-computing the
frame-by-frame transformations, overwriting any transformation files that already exist.

Data is saved in a folder of images, specified in the CSV file.

### CSV

Information needed for the registration is contained in the CSV. An example CSV file is provided, called `example_registrationInfo.csv`. The headers for this CSV are

  * `label`: a human-readable descriptor for this dataset. Need not be unique or anything
  * `subImageToRegister`: absolute path to the cropped subimage used to compute transformations. The format for this file can be anything that is opened with the `open` command
  in the [PIMS](http://soft-matter.github.io/pims/v0.4/) package for image I/O. I typically use Tiff stacks.
  * `fullImageToRegister`: absolute path to the full image to which to apply the transformations computed on the subimage.
  * `outputSuffix`: This is added to the end of the output folder name. Helpful to distinguish multiple runs of the registration pipeline
  * `firstFrame`: The first frame in the series to start tracking. 0-based indexing, so the very first frame is frame 0.
  * `lastFrame`: The last frame to register. Also 0-based indexing.
  * `xPosition`, `yPosition`: These coordinates relate the subimage to the full image. They indicate the position of the top left corner of the bounding box that defines
  the subimage within the full image. These are used to take the transformation computed on the subimage and convert it into the equivalent transform in the main image. These
  positions are in units of pixels and are 0-based indexing. The top left corner of an image is (x,y) = (0,0). x refers to horizontal coordinates (columns) and y refers to
  vertical coordinates (rows).
