function speed = computeSpeedOverTimeForKymographList(kymographList,rowsToAvg,varargin)
%%% Given a cell array of kymographs (kymographList),
%%% average the speed of each kymograph over a set distance from the wound.
%%% Kymograph: (1,1) is timepoint 0 closest to wound. Rows are distance
%%% from the wound, columns are timepoints.
%%% rowsToAvg: given in number of rows to average
%%% measureRelative (optional): If this extra flag is set, measure the
%%% relative speed (subtract the "far away" speed from the "nearby" speed.
%%% The boundary between "far away" and "nearby" is set by rowsToAvg
%%% Speed is a matrix, 1 row per kymograph, filled with NaNs for timepoitns
%%% that do not exist for a given kymograph.
%%% optional

%Parse arguments
p = inputParser;
addRequired(p, 'kymographList', @iscell);
addRequired(p, 'rowsToAvg', @(x) isnumeric(x) & x > 0);
addParameter(p, 'measureRelative', false, @islogical);
parse(p, kymographList, rowsToAvg, varargin{:});

kymographList = p.Results.kymographList;
rowsToAvg = p.Results.rowsToAvg;
measureRelative = p.Results.measureRelative;


nKymographs = numel(kymographList);
maxTimepoints = max(cellfun(@(x) size(x,2),kymographList));
speed = nan(nKymographs,maxTimepoints);

for k = 1:nKymographs
    kymograph = kymographList{k};
    nTimepoints = size(kymograph, 2);
    if measureRelative
        speed(k, 1:nTimepoints) = mean(kymograph(1:rowsToAvg, :), 1) ...
                                  - mean(kymograph(rowsToAvg+1:end, :), 1);
    else
    speed(k,1:nTimepoints) = mean(kymographList{k}(1:rowsToAvg,:),1); 
    end
end