function dataToPlot = processDataforVolumeOverTime(dataProcessingDetails,...
                                                plotDetails)
%%% Generate a plot of relative volume over time, with bootstrapped 95%
%%% confidence intervals for error bars, for a set of conditions.
%%% Data needs to be converted into a matrix with each row a different fish
%%% and each column a different timepoint. 

dataTable = readtable(dataProcessingDetails.volumeDataFile);
metaDataTable = readtable(dataProcessingDetails.metaDataFile);
dataToPlot = struct('condition', [], 'meanY', [], 'X', [],...
                     'XForCI', [], 'YForCI', [], 'displayCI', []);
             
%Data processing: gather observations for each condition in matrix format,
%bootraps confidence intervals, and collect the data

for k = 1:plotDetails.nConditions
    %select which datasetIDs to use (those that match 'condition'
    idToUse = unique(dataTable.datasetID(...
                 strcmp(dataTable.label,plotDetails.conditions(k).label)));
    %Find the max timepoint with data for this condition (across all cells)
    maxTimepoint = max(dataTable.time(ismember(dataTable.datasetID,idToUse)));
    %Initialize the matrix that will hold the relative volume over time
    relVolOverTime = nan(numel(idToUse),maxTimepoint);
    for m = 1:numel(idToUse)
        id = idToUse(m);
        %Absolute time in frames
        t = dataTable.time(dataTable.datasetID == id);
        absoluteVolume = dataTable.volumeInUM3(dataTable.datasetID == id);
        %Convert to 1-based index that is lined up based on first
        %post-wounding frame (anything <1 should not be used)
        relIdx = t - metaDataTable.firstPostWoundingTimepoint(metaDataTable.datasetID == id) + 1;
        preWoundVolumeIdx = find(t == metaDataTable.preWoundTimepoint(metaDataTable.datasetID == id), 1);
        %Normalize volume based on the most recent available volume prior
        %to wounding
        relativeVolume = absoluteVolume / absoluteVolume(preWoundVolumeIdx);
        %Align the volumes based on their timepoints, with the first
        %pre-wounding timepoint in column 1
        relVolOverTime(m, relIdx(relIdx>0)) = relativeVolume(relIdx>0);
    end
    %Any times that the cell has shrunk by over 80% that is a volume
    %measurement artifact
    relVolOverTime(relVolOverTime<0.2) = nan;
    %Trim off timepoints at the beginning or end that are all nan
    allNan = all(isnan(relVolOverTime),1);
    firstNonNan = find(~allNan,1);
    lastNonNan = find(~allNan, 1, 'last');
    relVolOverTime = relVolOverTime(:,firstNonNan:lastNonNan);
    %Compute the average relative volume over time
    meanRelVolume = nanmean(relVolOverTime(:,1:dataProcessingDetails.nTimepointsToShow), 1);
    %Get the confidence intervals for the volume measurements over time
    ciVolume = bootci(10000, @nanmean, relVolOverTime(:,1:dataProcessingDetails.nTimepointsToShow));
    %Time steps are taken from metadat, assuming that timesteps are in
    %minutes and timesteps are all the same for a given condition
    timeStep = (metaDataTable.timeStep(metaDataTable.datasetID == idToUse(1))/60);
    t = timeStep * (1:dataProcessingDetails.nTimepointsToShow);
    [tForCI, volForCI, displayCI] = choosePositionsForCI(t, meanRelVolume,...
                            ciVolume, k, plotDetails.nConditions);
    dataToPlot(k).condition = plotDetails.conditions(k).label;
    dataToPlot(k).meanY = meanRelVolume;
    dataToPlot(k).X = t;
    dataToPlot(k).XForCI = tForCI;
    dataToPlot(k).YForCI = volForCI;
    dataToPlot(k).displayCI = displayCI;
end
        
        