function ax = linePlotErrorBars(ax, dataToPlot, plotDetails)
%%% Create a plot involving multiple lines and error bars
dataHandles = [];
legendLabelList = {};
%Loop through the list of conditions and plot each in turn
for k = 1:plotDetails.nConditions
    disp(plotDetails.conditions(k).displayLabel)
    % Specify color for this condition
    colorToUse = (1/255) * plotDetails.conditions(k).color;
    % Specify a legend for this condition
    legendLabelList = [legendLabelList plotDetails.conditions(k).displayLabel];

    %Plot error bars only (spaced out on the plot)
    e = errorbar(dataToPlot(k).XForCI, dataToPlot(k).YForCI, ...
                 dataToPlot(k).displayCI(1,:), dataToPlot(k).displayCI(2,:),...
                 'Color', colorToUse,...
                 'LineStyle', 'none', 'Marker', 'None');
    e.CapSize = 0;
    e.LineWidth = 1;
    
    %Plot the mean value for this condition over time
    h=plot(dataToPlot(k).X, dataToPlot(k).meanY, ...
        'LineWidth',2, 'Color', colorToUse);
    %Add line to the list of lines to annotate with a legend at the end
    dataHandles = [dataHandles h];
end

[~,icons,~,~] = legend(dataHandles, legendLabelList, 'FontSize',9,...
                         'FontName','Arial');

shortenLegendLines(icons, plotDetails);
ax = basicFigureFormatting(ax, plotDetails);