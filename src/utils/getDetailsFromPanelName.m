function [dataProcessingDetails, plotDetails] = getDetailsFromPanelName(figureConfig, panelName)
%%% get structs that contain information for processing the data and
%%% plotting it.
%%% Input:
%%% - figureConfig is the function that contains the parameters for the given
%%% figure.
%%% - panelName is a string containing the figure number and panel, e.g.
%%% '2A'.
%%% Output:
%%% - dataProcessingDetails: a struct with infor for how to process the data
%%% for the figure (and where to find it)
%%% - plotDetails: details about the appearance of the plot (e.g. size,
%%% which conditions to plot, etc).

% Load the parameters
panelList = figureConfig();

% Use the panelName string to get the correct structs from the parameters
% above.
thisPanel = find(strcmp({panelList.panelName},panelName));
plotDetails = panelList(thisPanel).plotDetails;
dataProcessingDetails = panelList(thisPanel).dataProcessingDetails;

