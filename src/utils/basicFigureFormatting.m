function ax = basicFigureFormatting(ax,plotDetails,varargin)
%%% Basic formatting includes:
%%% - Setting x and y label
%%% - Setting x and y limits (if they are specified)
%%% - Setting font size
%%% - Setting the color of all axes lines to be proper black (rgb 0,0,0)
%%% - an optional flag indicates that there are multiple 
%%% Note: if there are multiple subplots, xlabel and ylabel might be a cell
%%% array of cell arrays. In this case it is important to check
p = inputParser;
addOptional(p, 'subplotToFormat', 1, @isnumeric);
parse(p, varargin{:});
subplotToFormat = p.Results.subplotToFormat;

ax.FontName = 'Arial';
ax.FontSize = 11;
ax.XColor = [0 0 0];
ax.YColor = [0 0 0];

%Handle multiple cases: a cell array of cell arrays (multiple subplots), or a cell array of
%strings (one multi-line xlabel) or just a string (a simple xlabel)
if isfield(plotDetails, 'xlabel')
    try
        if iscell(plotDetails.xlabel{1}) %cell array of cell arrays
            xlabel(ax, plotDetails.xlabel{subplotToFormat});
        else %just a multi-line label
            xlabel(ax, plotDetails.xlabel);
        end
    catch ME
        if strcmp(ME.identifier,'MATLAB:cellRefFromNonCell') % just a simple string
            xlabel(ax, plotDetails.xlabel);
        else
            rethrow(ME)
        end
    end
end
if isfield(plotDetails, 'ylabel')
    try
        if iscell(plotDetails.ylabel{1})
            ylabel(ax, plotDetails.ylabel{subplotToFormat});
        else %just a multi-line label
            ylabel(ax, plotDetails.ylabel);
        end
    catch ME
        if strcmp(ME.identifier,'MATLAB:cellRefFromNonCell') % just a simple string
            ylabel(ax, plotDetails.ylabel);
        else
            rethrow(ME)
        end
    end
end

%For the case of multiple subplots, have each subplot limits on its own row
if isfield(plotDetails,'ylim')
    ylim(ax,plotDetails.ylim(subplotToFormat,:));
end
if isfield(plotDetails, 'xlim')
    xlim(ax,plotDetails.xlim(subplotToFormat,:));
end
