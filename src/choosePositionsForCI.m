function [xDisplayCI,yDisplayCI,displayCI] = choosePositionsForCI(x,y,ci,k,N)
%%% For a trend graph x,y, (length nTotalDatapoints) with confidence intervals ci 
%%% (2xnTotalDatapoints),
%%% choose a subset of positions to plot the confidence intervals to allow
%%% easier spacing of multiple graphs with confidence intervals on the
%%% same plot.
%%% Inputs:
%%% x,y: Row vectors of nTotalDatapoints x- or y-coordinates, respectively. NB: Assume x
%%% is evenly spaced!
%%% ci: output from a confidence interval program. 2xnTotalDatapoints, row 1 contains the
%%% upper bound of ci (absolute, not relative to mean), and row 2 contains
%%% the (absolute) lower limit
%%% k: The index of this graph among the graphs to be displayed on the plot
%%% N: total number of graphs to be displayed on the same plot
%%% Output:
%%% xDisplayCI: Row vector of J( < nTotalDatapoins) x-coordinates to put confidence bars
%%% displayCI: 2xJ array of the RELATIVE positions of the confidence
%%% intervals at the chosen regions

% Goal: take the space between 2 datapoints in the original data and divide
% it into N intervals, and place a bar at each one
%Get x spacing
nTotalDatapoints = numel(x);

%Choose a reduced set of points to plot confidence intervals, such the
%confidence interval at a point is the one from the real data closest to
%where the confidence interval is being plotted.
indexOffset = ((2 * (k-1)) / N) + 1;
closestStartingIndex = round(indexOffset);

%First possibel list is just every other point starting at the starting
%index chosen above
possibleReducedIndexList = closestStartingIndex:2:nTotalDatapoints;
%Avoid extrapolation by truncating this possible list if necessary so that
%confidence intervals are only plotted in areas that are not interpolated.
nPointsToUse = floor(((nTotalDatapoints - indexOffset)/2)+1);
reducedIndexList = possibleReducedIndexList(1:nPointsToUse);

%Lift the offset from index coordinates to x coordinates
xOffset = interp1(1:numel(x),x,(0:2:numel(x)) + indexOffset,'linear',NaN);
xOffset(isnan(xOffset)) = [];
yOffset = interp1(x,y,xOffset);

%Select the cis and x-y coordinates at which to display confidence intervals
xDisplayCI = xOffset;
yDisplayCI = yOffset; %mean position for confidence interval

%Subtract the true y value from the confidence interval
displayCI = ci(:,reducedIndexList);
displayCI(1,:) = displayCI(1,:) - y(reducedIndexList);
displayCI(2,:) = y(reducedIndexList) - displayCI(2,:);


end

