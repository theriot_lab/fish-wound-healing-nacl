# Code used from FileExchange
For completeness, I am including the code from FileExchange that I have made use of, with their licenses. I am very grateful to the authors for freely sharing their code!

These projects include:
  * [plotSpread](https://www.mathworks.com/matlabcentral/fileexchange/37105-plot-spread-points-beeswarm-plot) for creating a beeswarm-style plot
  * [wanova](https://www.mathworks.com/matlabcentral/fileexchange/61661-wanova) for running the Welch's ANOVA
  * [games_howell](https://www.mathworks.com/matlabcentral/fileexchange/50452-pierremegevand-games_howell) for running the Games-Powell post-hoc tests
  * [bluewhitered](https://www.mathworks.com/matlabcentral/fileexchange/4058-bluewhitered) for creating a colormap with blue and red as extremes with a defined 0 position to be white
