function [score, coeff, latent] = runPCAOnData(inputData)
%%% Get a matrix that can be used for PCA analysis.
%%% Input - dataProcessingDetails, plotDetails are structs with minimum
%%% amounts of information about a specific figure
%%% inputData is a matrix with rows observations and columns dimensions
%%% output is a matrix, rows are observations, columns are different
%%% dimensions of the observation (e.g. timepoints)

%Make sure that the data is zero-mean along the columns
meanOverColumns = mean(inputData,1);
inputDataZeroMean = inputData - meanOverColumns;

[coeff, score, latent] = pca(inputDataZeroMean);
end