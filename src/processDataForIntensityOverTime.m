function dataToPlot = processDataForIntensityOverTime(dataProcessingDetails,...
                                               plotDetails)
%%% Generate a plot of LifeAct intensity over time, with bootstrapped 95%
%%% confidence intervals for error bars, for a set of conditions.
%%% Data is already in a matrix with each row a different fish and each
%%% column a different timepoint. So I just need to do the bootstrapping
%%% and picking of spacing of confidence intervals.
%%% Output:
%%% - X: horizontal axis (time in this case)
%%% - Y: vertical axis (intensity in this case)

load(dataProcessingDetails.dataFile,'allIntensityTrajectories',...
                                 'intensityConditionLookup');

dataToPlot = struct('condition', [], 'meanY', [], 'X', [],...
                     'XForCI', [], 'YForCI', [], 'displayCI', []);
                 
% Data processing: get the lines and confidence intervals to be plotted
for k = 1:plotDetails.nConditions
    %select which rows to use
    idxToUse = strcmp(intensityConditionLookup, plotDetails.conditions(k).label);
    thisConditionIntensity = allIntensityTrajectories(idxToUse, :);
    meanIntensity = nanmean(thisConditionIntensity, 1);
    
    t = dataProcessingDetails.dtInMinutes * (0:(size(meanIntensity,2) - 1));
    
    %Compute error bars with bootstrapping
    ciIntensity = bootci(10000, @nanmean, thisConditionIntensity);
    %Choose subset of error bars to plot
    [tForCI, intensityForCI, displayCI] = choosePositionsForCI(t, meanIntensity,...
                                           ciIntensity, k, plotDetails.nConditions);
    dataToPlot(k).condition = plotDetails.conditions(k).label;
    dataToPlot(k).meanY = meanIntensity;
    dataToPlot(k).X = t;
    dataToPlot(k).XForCI = tForCI;
    dataToPlot(k).YForCI = intensityForCI;
    dataToPlot(k).displayCI = displayCI;
end