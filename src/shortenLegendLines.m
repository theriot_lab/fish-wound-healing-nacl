function shortenLegendLines(icons, plotDetails)
%%% Using the icons struct generated from running the legend command,
%%% shorten the length of the legend lines (which are stored in icons). The
%%% first set of items in icons are the text objects, and after that are
%%% the lines themselves

for m = 1:numel(icons)
    %Re-position the text
    if isgraphics(icons(m), 'text')
        icons(m).Position = [plotDetails.legendTextLeftPosition,...
                         icons(m).Position(2) 0];
    %Reposition and shorten the lines themselves
    elseif isgraphics(icons(m), 'line')
        icons(m).XData = plotDetails.legendLineLength;
    else 
        continue
    end
end