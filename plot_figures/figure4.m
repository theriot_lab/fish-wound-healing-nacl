%% Fig. 4D: Velocity kymograph example
panelName = '4D';
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure4_parameters,...
                                                    panelName);

metaData = readtable(dataProcessingDetails.metaDataFile);

exptIndex = strcmp(metaData.label, plotDetails.label);

load(dataProcessingDetails.dataFile,'allVelocityXKymographs','kymographXCoordinates','kymographTCoordinates');

%For plotting reasons the spatial axis needs to be reversed
kymograph = allVelocityXKymographs{exptIndex}(end:-1:1,:);
t = kymographTCoordinates{exptIndex};
x = kymographXCoordinates{exptIndex};
electrodePosition = struct('Ant',[],'Post',[]);
electrodePosition.Ant  = metaData.anteriorElectrodePosition;
electrodePosition.Post = metaData.posteriorElectrodePosition;

fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on

imagesc([0, x(end)], [0, t(end)], kymograph(:,plotDetails.firstFrame:end)',...
        [-plotDetails.displayVelocity, plotDetails.displayVelocity]);
ax = gca;
ax.YDir = 'Reverse';
%Choose direction anterior (L) to posterior (R)
%ax.XDir = 'Reverse';
%Plot the first cathode position
timeVec = [0,0];
timeVec(1) = 0.5*(metaData.frameFieldOn(exptIndex) - 1);
if metaData.frameFieldSwitch(exptIndex) > 0 
    timeVec(2) = 0.5*(metaData.frameFieldSwitch(exptIndex) - 1);
elseif metaData.frameFieldOff(exptIndex) > 0
    timeVec(2) = 0.5*(metaData.frameFieldOff(exptIndex) - 1);
else
    timeVec(2) = t(end);
end
if strcmp(metaData.cathodeBeforeSwitch{exptIndex}, 'Anterior')
    cathodeVec = metaData.micronsPerPixel(exptIndex) ...
           * metaData.anteriorElectrodePosition(exptIndex) * [1, 1];
elseif strcmp(metaData.cathodeBeforeSwitch{exptIndex}, 'Posterior')
    cathodeVec = metaData.micronsPerPixel(exptIndex) ...
           * metaData.posteriorElectrodePosition(exptIndex) * [1, 1];
else
    cathodeVec = [-1,-1];
end
plot(cathodeVec,timeVec,'k:o','LineWidth',4,'MarkerSize',10,...
          'MarkerFaceColor',(1/255) * plotDetails.cathodeMarkerColor);
%Plot the second cathode position (if it exists)
if metaData.frameFieldSwitch(exptIndex) > 0
    timeVec(1) = 0.5 * (metaData.frameFieldSwitch(exptIndex) - 1);
    if metaData.frameFieldOff(exptIndex) > 0
        timeVec(2) = 0.5 * (metaData.frameFieldOff(exptIndex) - 1);
    else
        timeVec(2) = t(end);
    end
    if strcmp(metaData.cathodeAfterSwitch{exptIndex}, 'Anterior')
        cathodeVec = metaData.micronsPerPixel(exptIndex) ...
                   * metaData.anteriorElectrodePosition(exptIndex) * [1, 1];
    elseif strcmp(metaData.cathodeAfterSwitch{exptIndex}, 'Posterior')
        cathodeVec = metaData.micronsPerPixel(exptIndex) ...
                   * metaData.posteriorElectrodePosition(exptIndex) * [1, 1];
    else
        cathodeVec = [-1, -1];
    end
    plot(cathodeVec, timeVec, 'k:o', 'LineWidth', 4, 'MarkerSize', 10, ...
            'MarkerFaceColor', (1/255) * plotDetails.cathodeMarkerColor);
end

colormap(gca,bluewhitered(256))
cc = colorbar('southoutside');
ccTitleHandle = get(cc,'Title');
set(ccTitleHandle,'String',plotDetails.colorbarLabel,'FontSize',12);

%% Fig. 4E: Velocity averaged at different points
panelName = '4E';
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure4_parameters,...
                                                    panelName);

metaData = readtable(dataProcessingDetails.metaDataFile);
%-----------------------------------------------
%Process data - get the median horizontal velocity component between the
%electrodes and then take the median over the time intervals in each
%defined phase - before Efield on (1), efield on (2), efield switched 
%polarity (3), and efield turned off again (4).
%Collect the median velocity in each phase
velocity4phase = nan(4, plotDetails.nLarvae);

for k = 1:plotDetails.nLarvae
    idx = strcmp(metaData.label, plotDetails.larvaeToPlot{k});
    clear coordinateArray
    pathToLoad = sprintf('%s/%scoordinateArray.mat',...
                         metaData.pathToData{idx},...
                         metaData.dataPrefix{idx});
    load(pathToLoad, 'coordinateArray');
    %Convert pixels/frame to microns/minute
    velocityX = 2*metaData.micronsPerPixel(idx) * (coordinateArray(:, 3:2:end)...
                                             - coordinateArray(:, 1:2:end-2));
    coordinateX = coordinateArray(:, 1:2:end); %in pixels
    antPosition = metaData.anteriorElectrodePosition(idx); %in pixels
    postPosition = metaData.posteriorElectrodePosition(idx); %in pixels
    betweenElectrodes = (coordinateX > (antPosition ...
                          + dataProcessingDetails.pixelsAwayFromElectrode)) ...
                      & (coordinateX < (postPosition ... 
                          - dataProcessingDetails.pixelsAwayFromElectrode));
    %ignore feature points that do not lie between the electrodes
    velocityX(~betweenElectrodes(:,1:end-1)) = nan;
    medianVelocity = nanmedian(velocityX,1); %median at each timepoint

    framesAwayFromSwitch = dataProcessingDetails.framesAwayFromSwitch;
    phase = cell(4,1);
    %phase 1: before Efield first turned on
    phase{1} = framesAwayFromSwitch : (metaData.frameFieldOn(idx) - framesAwayFromSwitch);
    %phase 2: after Efield has been turned on
    phase{2} = (metaData.frameFieldOn(idx) + framesAwayFromSwitch) : ...
             (metaData.frameFieldSwitch(idx) - framesAwayFromSwitch);
    %phase 3: Once the Efield has been switched
    phase{3} = (metaData.frameFieldSwitch(idx) + framesAwayFromSwitch) : ...
             (metaData.frameFieldOff(idx) - framesAwayFromSwitch);
    %phase 4: When the Efield has been turned off again
    phase{4} = (metaData.frameFieldOff(idx) + framesAwayFromSwitch) : ...
             size(medianVelocity, 2);
         
    for p = 1:4
        %Compute median across timepoints
        velocity4phase(p, k) = median(medianVelocity(phase{p}));
    end
end

%----------------------------------
%Plot the data

fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on

handleList = [];
legendList = {};
%Keep track of how many larvae of each type are being counted
antFirstCounter = 0;
postFirstCounter = 0;
%Create a grey zone where the electric field is on
fill([-2.2, 2.2, 2.2, -2.2], [3.5, 3.5, 1.5, 1.5], 0.9 * [1, 1, 1],...
     'EdgeColor', 'none');
%Plot a line through 0 net velocity
plot([0, 0], [0.8, 4.2], 'k-', 'LineWidth', 1);
for k = 1:plotDetails.nLarvae
    idx = strcmp(metaData.label, plotDetails.larvaeToPlot{k});
    %Pick line colors
    if strcmp(metaData.cathodeBeforeSwitch{idx},'Anterior')
        c = (1/255) * plotDetails.colorCathodeAnteriorFirst;
        displayLabel = plotDetails.legendCathodeAnteriorFirst;
        antFirstCounter = antFirstCounter + 1;
    elseif strcmp(metaData.cathodeBeforeSwitch{idx}, 'Posterior')
        c = (1/255) * plotDetails.colorCathodePosteriorFirst;
        displayLabel = plotDetails.legendCathodePosteriorFirst;
        postFirstCounter = postFirstCounter + 1;
    else %an error!
        c = [1, 0, 0];
    end
    h = plot(velocity4phase(:,k), (4:-1:1)', '--', 'color', c, ...
         'LineWidth', 1, 'Marker', 'o', 'MarkerSize',7);
    if ~ismember(displayLabel, legendList)
        handleList = [handleList, h];
        legendList = [legendList, displayLabel];
    end
end
%Adjust the legend entries with the number of larvae in each condition
m = strcmp(plotDetails.legendCathodeAnteriorFirst, legendList);
legendList{m} = sprintf('%s (N = %d)', plotDetails.legendCathodeAnteriorFirst,...
                          antFirstCounter);
m = strcmp(plotDetails.legendCathodePosteriorFirst, legendList);
legendList{m} = sprintf('%s (N = %d)', plotDetails.legendCathodePosteriorFirst,...
                          postFirstCounter);
legend(handleList, legendList, 'FontSize', 9);
yticks([1, 2, 3, 4]);
ax = gca;
ax = basicFigureFormatting(ax, plotDetails);

%% Figure 4F: Stimulated vs Unstimulated velocity components
panelName = "4F";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure4_parameters,...
                                                    panelName);
load(dataProcessingDetails.dataFile,'allVelocityXKymographs','kymographTCoordinates');
metaData = readtable(dataProcessingDetails.metaDataFile);
velocityX = computeSpeedOverTimeForKymographList(allVelocityXKymographs,...
                dataProcessingDetails.rowsToAvg);
%Restrict the amount of timepoints to show
nT = dataProcessingDetails.timepointsToShow;
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
handleList = zeros(1, plotDetails.nConditions);
%Plot a line for 0 velocity
plot([0,0], plotDetails.ylim, 'k-','LineWidth',0.5)
for k = 1:plotDetails.nConditions
    thisCondition = find(strcmp(metaData.experimentalTreatmentPreWounding, ...
                             plotDetails.conditions(k).label));    
    %Plot each individual trace
    for m = 1:numel(thisCondition)
        idx = thisCondition(m);
        plot(velocityX(idx,1:nT), kymographTCoordinates{idx}(1:nT)',...
             'Color', (1/255) * plotDetails.conditions(k).color, ...
             'LineWidth', 0.5);
    end
    %Plot the average of all the traces
    vX = velocityX(thisCondition, 1:nT);
    h = plot(mean(vX,1), 0.5*(0:(nT-1)), 'Color', (1/255) * plotDetails.conditions(k).color,...
         'LineWidth', 2);
    handleList(k) = h;
end
ax.YDir = 'reverse';
ax = basicFigureFormatting(ax, plotDetails);
legend(handleList, {plotDetails.conditions.displayLabel})


