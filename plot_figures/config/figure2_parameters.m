function panelList = figure2_parameters()
%%% Define parameters for figure 2

idx = 0; %counter to keep track of figure panels
%Change directories to make sure you are in the plot_figures directory (which
%is always above the directory of the parameters!)
cd(fullfile(fileparts(mfilename('fullpath')), '..'));
%Initialize a struct to hold information about each figure panel
panelList = struct('panelName', [],...
                   'dataProcessingDetails', [],...
                   'plotDetails', []);

%% Fig 2A: 
idx = idx + 1;
panelList(idx).panelName = '2A';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 40; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 234;
%set panel height
plotDetails.figureSize.height = 158;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Average speed (�m/min)';
%Set the y axis limit
plotDetails.ylim = [0,9];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded (E3; n = 3)';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo (E3; n =8)';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl (n = 10)';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl (n = 7)';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluconate (n = 7)';

plotDetails.conditions(6).label = 'KCl135mM';
plotDetails.conditions(6).color = [31,203,255];
plotDetails.conditions(6).displayLabel = 'Iso KCl (n = 8)';

plotDetails.conditions(7).label = 'Sorbitol270mM';
plotDetails.conditions(7).color = [169,131,253];
plotDetails.conditions(7).displayLabel = 'Iso Sorbitol (n = 8)';
%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig. 2B
idx = idx + 1;
panelList(idx).panelName = '2B';

%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 30; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;
%define structures to hold data for this figure
plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]));
%set panel width
plotDetails.figureSize.width = 278;
%set panel height
plotDetails.figureSize.height = 156;
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluc';

plotDetails.conditions(6).label = 'KCl135mM';
plotDetails.conditions(6).color = [31,203,255];
plotDetails.conditions(6).displayLabel = 'Iso KCl';

plotDetails.conditions(7).label = 'Sorbitol270mM';
plotDetails.conditions(7).color = [169,131,253];
plotDetails.conditions(7).displayLabel = 'Iso Sorbitol';
%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
%set the x and y limits (x is somewhat arbitrary)
plotDetails.xlim = [0.5, 7.5];
plotDetails.ylim = [-10, 16];
%set the ylabel (no x label, just the tick marks)
plotDetails.ylabel = 'PC1 Score';
panelList(idx).plotDetails = plotDetails;

%% Fig. 2E: LifeAct Intensity after non-rigid registration
idx = idx + 1;
panelList(idx).panelName = '2E';

%define structures to hold info about processing data for this figure
dataProcessingDetails = struct('dataFile',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '20200713_LifeActFF0.mat';
%Specify the spacing between timepoints
dataProcessingDetails.dtInMinutes = 0.5;

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 184;
%set panel height
plotDetails.figureSize.height = 145;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'LifeAct \DeltaF/F_0';
%Set x and y axis limits
plotDetails.xlim = [0,15];
plotDetails.ylim = [-0.05, 0.5];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = 'Hypo (E3; n = 6)';

plotDetails.conditions(2).label = 'NaCl135mM';
plotDetails.conditions(2).color = [135,36,22];
plotDetails.conditions(2).displayLabel = 'Iso NaCl (n = 6)';

plotDetails.conditions(3).label = 'CholineCl135mM';
plotDetails.conditions(3).color = [59,142,144];
plotDetails.conditions(3).displayLabel = 'Iso CholineCl (n = 7)';

plotDetails.conditions(4).label = 'NaGluconate135mM';
plotDetails.conditions(4).color = [208,117,40];
plotDetails.conditions(4).displayLabel = 'Iso NaGluconate (n = 6)';

plotDetails.conditions(5).label = 'KCl135mM';
plotDetails.conditions(5).color = [31,203,255];
plotDetails.conditions(5).displayLabel = 'Iso KCl (n = 7)';

plotDetails.conditions(6).label = 'Sorbitol270mM';
plotDetails.conditions(6).color = [169,131,253];
plotDetails.conditions(6).displayLabel = 'Iso Sorbitol (n = 6)';
%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;


%% Fig. 2F: sodium concentration in two-chamber device
idx = idx + 1;
panelList(idx).panelName = '2F';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'imageFile',[],...
    'concentrationCalibration',[]);

%Specify location of .mat file with the baseline-subtracted,
%flat-field corrected image of the fish in the two chamber device
dataProcessingDetails.imageFile = 'movie_stitched_baseline_flatfield_088.mat';
%Specify the location of the .csv file with the intensity calibration.
%Will be used to construct a binding curve
dataProcessingDetails.concentrationCalibration = '20200715_CoroNaGreenCalibrationTable.csv';
%Specify the location of the line profile (made in ImageJ based on the
%calibrated image above).
dataProcessingDetails.concentrationLineScan = 'ConcentrationLineScan_20200224.csv';
%Choose an initial guess for the nonlinear fit of fluorescence intensity to
%a binding curve of the form (Imax * c)/(Kd + c)
dataProcessingDetails.initialGuess = [125, 1]; %[Kd, Imax]
%Specify the binding curve model to be fit
dataProcessingDetails.bindingCurve = @(b,x)((b(2) * x) ./ (b(1) + x));
%Specify the inverse equation to get concentration from intensity.
%[b(1), b(2)] is [Kd, Imax].
dataProcessingDetails.inverseBindingCurve = @(b,y)((b(1) * y) ./ (b(2) - y));

panelList(idx).dataProcessingDetails = dataProcessingDetails;


%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    ...%Choose the range of concentrations to display with color
    'concentrationDisplayRange', [],...
    'imageSubplotPosition',[],...
    'linescanSubplotPosition',[]);

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 212;
%set panel height
plotDetails.figureSize.height = 120;
%set the dynamic range of concentrations (in mM) to display with the colormap
plotDetails.concentrationDisplayRange = [0 150];
%Set x and y axis labels (for the linescan)
plotDetails.xlabel = 'Position (�m)';
plotDetails.ylabel = '[Na^+] (mM)';
%Set the x and y axis limits (for the linescan)
plotDetails.ylim = [0, 130];
%Choose an amount (in pixels) from the top and bottom of the image to crop
plotDetails.cropRows = 300;
%Specify the precise positioning of both the image and linescan subplots.
%Note that the image position will be revised to maintain the aspect ratio
%of the image
plotDetails.imageSubplotPosition = [0.3,0.5,0.65,0.4288];
plotDetails.linescanSubplotPosition = [0.3,0.15,0.65,0.3412];
%Specify the title on the concentration colorbar
plotDetails.colorbarLabel = '[Na^+] (mM)';
panelList(idx).plotDetails = plotDetails;

%% Fig. 2G: Relative velocity two-chamber experiment 
idx = idx + 1;
panelList(idx).panelName = '2G';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200714_TwoChambers_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200714_TwoChambers_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 40; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 212;
%set panel height
plotDetails.figureSize.height = 151;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Relative speed (�m/min)';
%Set x and y limits
plotDetails.xlim = [0,20];
plotDetails.ylim = [-0.3, 3.5];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'AntE3_PostE3';
plotDetails.conditions(1).color = [0, 0, 0];
plotDetails.conditions(1).displayLabel = 'Hypo/Hypo (n = 5)';

plotDetails.conditions(2).label = 'AntNaCl135mM_PostE3';
plotDetails.conditions(2).color = [154, 154, 154];
plotDetails.conditions(2).displayLabel = 'Iso/Hypo (n = 4)';

plotDetails.conditions(3).label = 'AntNaCl135mM_PostNaCl135mM';
plotDetails.conditions(3).color = [255, 0, 0];
plotDetails.conditions(3).displayLabel = 'Iso/Iso (n = 4)';

plotDetails.conditions(4).label = 'AntE3_PostNaCl135mM';
plotDetails.conditions(4).color = [255, 154, 154];
plotDetails.conditions(4).displayLabel = 'Hypo/Iso (n = 4)';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig. S2A-D: PCA on speed over time (4 related subplots)
idx = idx + 1;
panelList(idx).panelName = 'S2A-D';

%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 30; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;
%define structures to hold data for this figure
plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]));
%set panel width
plotDetails.figureSize.width = 493;
%set panel height
plotDetails.figureSize.height = 150;
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluc';

plotDetails.conditions(6).label = 'KCl135mM';
plotDetails.conditions(6).color = [31,203,255];
plotDetails.conditions(6).displayLabel = 'Iso KCl';

plotDetails.conditions(7).label = 'Sorbitol270mM';
plotDetails.conditions(7).color = [169,131,253];
plotDetails.conditions(7).displayLabel = 'Iso Sorbitol';
%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
%Specify the precise positions of each of the 4 subplots
plotDetails.subplotPosition = [0.05, 0.2, 0.18, 0.7; %1st PC variation
                               0.31, 0.2, 0.18, 0.7; %2nd PC variation
                               0.58, 0.2, 0.14, 0.7; %variance explained by PC
                               0.79, 0.2, 0.2,  0.5]; %scatterplot first 2 PCs
plotDetails.subplotTitle_1 = 'PC 1';
plotDetails.subplotTitle_2 = 'PC 2';
plotDetails.xlabel = {{'Time post wounding'; '(min)'},...
                          {'Time post wounding'; '(min)'},...
                          {'PC'},...
                          {'PC 1 Score'}};
plotDetails.ylabel = {{'Speed (�m/min)'},...
                          {'Speed (�m/min)'},...
                          {'% Variance Explained'},...
                          {'PC 2 Score'}};
plotDetails.ylim = [0, 6;
                    0, 6;
                    0, 80
                    -10,7];
plotDetails.xlim = [0, 14.5;
                    0, 14.5;
                    0, 10;
                    -9.5, 15];
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;
%Specify the position of the legend in the 4th plot (size will be
%determined automatically)
plotDetails.legend4XY = [0.8, 0.3];
panelList(idx).plotDetails = plotDetails;

%% Fig. S2F: Alpha bungarotoxin speed over time
idx = idx +1;
panelList(idx).panelName = 'S2F';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...%%%
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 40; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 234;
%set panel height
plotDetails.figureSize.height = 158;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Average speed (�m/min)';
%Set the y axis limit
plotDetails.ylim = [0,9];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = 'Hypo (E3; n =8)';

plotDetails.conditions(2).label = 'NaCl135mM';
plotDetails.conditions(2).color = [135,36,22];
plotDetails.conditions(2).displayLabel = 'Iso NaCl (n = 10)';

plotDetails.conditions(3).label = 'aBungaro_mRNA_noTri_IsoNaCl';
plotDetails.conditions(3).color = [248, 142, 197];
plotDetails.conditions(3).displayLabel = '\alpha-bungarotoxin, Iso NaCl (n = 5)';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;
end