function panelList = figure1_parameters()
%%% Define parameters for figure 1

%Change directories to make sure you are in the plot_figures directory (which
%is always above the directory of the parameters!)
cd(fullfile(fileparts(mfilename('fullpath')), '..'));
%Initialize a struct to hold information about each figure panel
panelList = struct('panelName', [],...
                   'dataProcessingDetails', [],...
                   'plotDetails', []);
% Define counter to keep track of the current entry in panel list
idx = 0;

%% Fig. 1E: averaged speed kymograph
idx = idx + 1;
panelList(idx).panelName = '1E';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 40; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 40; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[]);

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 400;
%set panel height
plotDetails.figureSize.height = 400;
%Resize the average kymograph (in normalized units)
plotDetails.subplotPosition_1 = [0.15, 0.3, 0.54,  0.54];
%Resize the side marginal plot (net displacements over different positions)
plotDetails.subplotPosition_2 = [0.75, 0.3, 0.127, 0.54];
%Resize the bottom marginal plot (net displacements over time)
plotDetails.subplotPosition_3 = [0.15, 0.1, 0.54,  0.127];
%Choose the condition to visualize kymographs for
plotDetails.condition = 'none'; %'none' is the fish in Hypo (E3) medium
%Set the speed range for the heat map ([cmin,cmax]. nan uses the default of
%[0, max(kymograph(:))])
plotDetails.colorbarRange = nan;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
%label for the kymograph
plotDetails.ylabel_1 = {'Initial distance';
                      'from wound (�m)'};
%label for the side marginal plot
plotDetails.xlabel_2 = {'Displacement';'over 20 min';'(�m)'};
%label for the bottom marginal plot
plotDetails.xlabel_3 = 'Time post wounding (min)';
plotDetails.ylabel_3 = {'Net displacement';'(�m)'};
plotDetails.colorbarLabel = 'Speed (�m/min)';

panelList(idx).plotDetails = plotDetails;

%% Fig. 1G: GCaMP intensity kymograph
idx = idx + 1;
panelList(idx).panelName = '1G';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200713_all_gcamp_kymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200123_GCaMPDetails.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 40; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 80; %Each timepoint is 15 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[]);

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 200;
%set panel height
plotDetails.figureSize.height = 200;
%Set the speed range for the heat map ([cmin,cmax]. nan uses the default of
%[0, max(kymograph(:))])
plotDetails.colorbarRange = nan;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = {'Distance';'from wound (�m)'};
plotDetails.colorbarLabel = 'GCaMP6f intensity (\DeltaF/F_0)';

panelList(idx).plotDetails = plotDetails;

%% Fig 1H: normalized average tracks over time
idx = idx + 1;
panelList(idx).panelName = '1H';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200505_normalizedTracksOverTime.mat';
% Set the number of timepoints to show in the plot
dataProcessingDetails.minToShow = 19.5;

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[],...
                         'tVecName',[],'normalizedTrackName',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 230;
%set panel height
plotDetails.figureSize.height = 98;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Value relative to max';
plotDetails.ylim = [0,1];
plotDetails.xlim = [0,20];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'speed';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = 'Speed';
plotDetails.conditions(1).tVecName = 'tSpeed';
plotDetails.conditions(1).normalizedTrackName = 'normalizedSpeed';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,170,0];
plotDetails.conditions(2).displayLabel = 'GCaMP intensity';
plotDetails.conditions(2).tVecName = 'tGCaMP';
plotDetails.conditions(2).normalizedTrackName = 'normalizedGCaMP';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig S1B: Laceration vs. Tail Transection
idx = idx + 1;
panelList(idx).panelName = 'S1B';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200517_TailTransectionData_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200517_TailTransectionData_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 40; %Each timepoint is 30 seconds


panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[],...
                         'tVecName',[],'normalizedTrackName',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 182;
%set panel height
plotDetails.figureSize.height = 150;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Speed (�m/min)';
plotDetails.ylim = [0,9];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = 'Laceration (n = 8)';

plotDetails.conditions(2).label = 'TailTransection';
plotDetails.conditions(2).color = [255,0,0];
plotDetails.conditions(2).displayLabel = 'Tail transection (n = 6)';


%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig. S1C: examples of tracking
idx = idx + 1;
panelList(idx).panelName = 'S1C';

%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'imageFile',[],...
    'coordinateFile',[]);

% Location of the example movie
dataProcessingDetails.imageFile = ['../data/20x_untreated_Wounding_2_MaxZ_',...
                                 'registered_20190404_cropped_LifeAct.tif'];
% Location of the tracked points for this file
dataProcessingDetails.coordinateFile = ['../data/',...
                            'movie_09_trackingRun001_coordinateArray.mat'];
% Choose a random seed for reproducibly selecting a subset of points to
% plot
dataProcessingDetails.rSeed = 11;
% Choose the fraction of initial points to show
dataProcessingDetails.fracInitialPoints = 0.15;
% Choose the number of points to show the full tracks of
dataProcessingDetails.nTracks = 10;
% Choose the fraction of points to color-code by displacement
dataProcessingDetails.fracTrackedPoints = 0.25;
% For simplicity, a x-pixel coordinate is chosen for this movie such that
% any point to the right of this pixel is near the wound (for illustration
% purposes)
dataProcessingDetails.nearPointThreshold = 380;

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', [])... %figure size in pts
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 800;
%set panel height
plotDetails.figureSize.height = 200;
plotDetails.subplotPosition_1 = [0.13, 0.2, 0.2134, 0.7];
plotDetails.subplotPosition_2 = [0.4108, 0.2, 0.2134, 0.7];
plotDetails.subplotPosition_3 = [0.6916, 0.2, 0.2134, 0.7];
plotDetails.subplotPosition_5 = [0.4108, 0.1, 0.2134, 0.1];
plotDetails.subplotPosition_6 = [0.6916, 0.1, 0.2134, 0.1];

panelList(idx).plotDetails = plotDetails;
end