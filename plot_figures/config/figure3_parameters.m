function panelList = figure3_parameters()
%%% Define parameters for figure 3

%Change directories to make sure you are in the plot_figures directory (which
%is always above the directory of the parameters!)
cd(fullfile(fileparts(mfilename('fullpath')), '..'));
%Initialize a struct to hold information about each figure panel
panelList = struct('panelName', [],...
                   'dataProcessingDetails', [],...
                   'plotDetails', []);
% Define counter to keep track of the current entry in panel list
idx = 0;

%% Fig. 3B: Relative volume over time
idx = idx + 1;
panelList(idx).panelName = '3B';

dataProcessingDetails = struct('volumeDataFile',[]);
%Specifically use the dataset with volumes over time
dataProcessingDetails.volumeDataFile = '../data/20200719_absoluteVolumeOverTime.csv';
dataProcessingDetails.metaDataFile = '../data/20200719_VolumeMetadata.csv';
%Choose how many timepoints (after wounding) to show
dataProcessingDetails.nTimepointsToShow = 22;
panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]),...
        'xlabel',[],'ylabel',[]);

%set panel width
plotDetails.figureSize.width = 234;
%set panel height
plotDetails.figureSize.height = 162;
%Set the x and y labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Relative volume';
%Set the x and y limits
plotDetails.xlim = [0, 17];
plotDetails.ylim = [0.9, 1.8];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluc';

plotDetails.nConditions = numel(plotDetails.conditions);

% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig. 3C part 1
idx = idx + 1;
panelList(idx).panelName = '3C1';
dataProcessingDetails = struct('volumeDataFile',[]);
%Specifically use the dataset with just before/after volumes
dataProcessingDetails.volumeDataFile = '../data/20200505_initialVolumeTable.csv';

panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]),...
        'xlabel',[],'ylabel',[]);

%set panel width
plotDetails.figureSize.width = 240;
%set panel height
plotDetails.figureSize.height = 200;
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluc';

plotDetails.xlabel = 'condition';
plotDetails.ylabel = 'Volume of cell cluster (pL)';
plotDetails.nConditions = numel(plotDetails.conditions);

panelList(idx).plotDetails = plotDetails;

%% Fig. 3C part 2: mean paired differences
idx = idx + 1;
panelList(idx).panelName = '3C2';
dataProcessingDetails = struct('volumeDataFile',[]);
dataProcessingDetails.volumeDataFile = '../data/20200505_initialVolumeTable.csv';

panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]),...
        'xlabel',[],'ylabel',[]);

%set panel width
plotDetails.figureSize.width = 200;%130;
%set panel height
plotDetails.figureSize.height = 104;%140;
%Set label

% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'Iso NaCl';

plotDetails.conditions(4).label = 'CholineCl135mM';
plotDetails.conditions(4).color = [59,142,144];
plotDetails.conditions(4).displayLabel = 'Iso CholineCl';

plotDetails.conditions(5).label = 'NaGluconate135mM';
plotDetails.conditions(5).color = [208,117,40];
plotDetails.conditions(5).displayLabel = 'Iso NaGluc';

plotDetails.xlabel = 'Mean paired difference (pL)';
plotDetails.nConditions = numel(plotDetails.conditions);

panelList(idx).plotDetails = plotDetails;

%% Fig 3D: 
idx = idx + 1;
panelList(idx).panelName = '3D';
%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);

%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.dataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.metadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 30; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 147;
%set panel height
plotDetails.figureSize.height = 130;
%Set x and y axis labels
plotDetails.xlabel = 'Time post wounding (min)';
plotDetails.ylabel = 'Average speed (�m/min)';
%Set y axis limit
plotDetails.ylim = [0,9];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = '5mM (Hypo)';

plotDetails.conditions(2).label = 'NaCl135mM';
plotDetails.conditions(2).color = [135,36,22];
plotDetails.conditions(2).displayLabel = '135mM (Iso)';

plotDetails.conditions(3).label = 'NaCl100mM';
plotDetails.conditions(3).color = [240, 29, 0];
plotDetails.conditions(3).displayLabel = '100mM';

plotDetails.conditions(4).label = 'NaCl068mM';
plotDetails.conditions(4).color = [255,151,138];
plotDetails.conditions(4).displayLabel = '68mM';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;

%% Fig. 3E: Relative volume over time for sodium chloride
idx = idx + 1;
panelList(idx).panelName = '3E';

dataProcessingDetails = struct('volumeDataFile',[]);
%Specifically use the dataset with volumes over time
dataProcessingDetails.volumeDataFile = '../data/20200719_absoluteVolumeOverTime.csv';
dataProcessingDetails.metaDataFile = '../data/20200719_VolumeMetadata.csv';
%Choose how many timepoints (after wounding) to show
dataProcessingDetails.nTimepointsToShow = 18;
panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]),...
        'xlabel',[],'ylabel',[]);
%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 147;
%set panel height
plotDetails.figureSize.height = 144;
%Set x and y axis labels
plotDetails.xlabel = {'Time post'; 'wounding (min)'};
plotDetails.ylabel = 'Relative volume';
%Set y axis limit
plotDetails.xlim = [0,14];
plotDetails.ylim = [0.9, 1.8];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = '5mM (Hypo)';

plotDetails.conditions(2).label = 'NaCl135mM';
plotDetails.conditions(2).color = [135,36,22];
plotDetails.conditions(2).displayLabel = '135mM (Iso)';

plotDetails.conditions(3).label = 'NaCl100mM';
plotDetails.conditions(3).color = [240, 29, 0];
plotDetails.conditions(3).displayLabel = '100mM';

plotDetails.conditions(4).label = 'NaCl068mM';
plotDetails.conditions(4).color = [255,151,138];
plotDetails.conditions(4).displayLabel = '68mM';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;


%% Fig. 3F
idx = idx + 1;
panelList(idx).panelName = '3F';

%define structures to hold info about processing data for this figure
dataProcessingDetails = struct(...
    'dataFile',[],...
    'metadataFile',[],...
    'rowsToAvg',[],...
    'timepointsToShow',[]);
%Choose the location where .mat file with kymograph data is stored
dataProcessingDetails.speedDataFile = '../data/20200415_AllDataForPaper_allKymographs.mat';
%Choose the location where the .csv file with metadata is stored
dataProcessingDetails.speedMetadataFile = '../data/20200415_AllDataForPaper_DataRecord.csv';
%Choose the location of the .csv of the before- and after cell volumes
dataProcessingDetails.volumeDataFile = '../data/20200505_initialVolumeTable.csv';
% Set number of rows to average to get speed over time
dataProcessingDetails.rowsToAvg = 30; %each row represents 10�m
% Set the number of timepoints to show in the plot
dataProcessingDetails.timepointsToShow = 30; %Each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
      ...%number of separate conditions to be plotted (computed from the list
        'nConditions', [],...
     ...%Plotting information for each condition: label to use to access the
     ...%data, color to plot the data, and the label to display in the legend
        'conditions', struct('label',[],'color',[],'displayLabel',[]),...
        'xlabel',[],'ylabel',[]);
%set panel width
plotDetails.figureSize.width = 130;
%set panel height
plotDetails.figureSize.height = 140;
%set axis labels
plotDetails.xlabel = {'Relative initial volume'};
plotDetails.ylabel = 'Speed PC 1';
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'noWound';
plotDetails.conditions(1).color = [62,42,144];
plotDetails.conditions(1).displayLabel = 'Unwounded';

plotDetails.conditions(2).label = 'none';
plotDetails.conditions(2).color = [0,0,0];
plotDetails.conditions(2).displayLabel = 'Hypo';

plotDetails.conditions(3).label = 'NaCl135mM';
plotDetails.conditions(3).color = [135,36,22];
plotDetails.conditions(3).displayLabel = 'NaCl 135mM (Iso)';

plotDetails.conditions(5).label = 'NaCl068mM';
plotDetails.conditions(5).color = [255,151,138];
plotDetails.conditions(5).displayLabel = 'NaCl 68mM';

plotDetails.conditions(4).label = 'NaCl100mM';
plotDetails.conditions(4).color = [240,29,0];
plotDetails.conditions(4).displayLabel = 'NaCl 100mM';

plotDetails.conditions(6).label = 'CholineCl135mM';
plotDetails.conditions(6).color = [59,142,144];
plotDetails.conditions(6).displayLabel = 'Iso CholineCl';

plotDetails.conditions(7).label = 'NaGluconate135mM';
plotDetails.conditions(7).color = [208,117,40];
plotDetails.conditions(7).displayLabel = 'Iso NaGluc';
%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);

panelList(idx).plotDetails = plotDetails;
