function panelList = figure4_parameters()
%%% Define parameters for figure 4

%Change directories to make sure you are in the plot_figures directory (which
%is always above the directory of the parameters!)
cd(fullfile(fileparts(mfilename('fullpath')), '..'));
%Initialize a struct to hold information about each figure panel
panelList = struct('panelName', [],...
                   'dataProcessingDetails', [],...
                   'plotDetails', []);
% Define counter to keep track of the current entry in panel list
idx = 0;

%% Fig. 4D: Velocity kymograph example
idx = idx + 1;
panelList(idx).panelName = '4D';

dataProcessingDetails = struct('dataFile',[],...
                               'metaDataFile',[]);
dataProcessingDetails.dataFile = '../data/20200803_stimulation_allVelocityXKymographs.mat';
dataProcessingDetails.metaDataFile = '../data/20200722_stimulation_DataRecord.csv';
panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
     ...%Choose which experiment to plot, by label (matching to the metadata)
        'label', [],...
        'xlabel',[],'ylabel',[]);
%set panel width
plotDetails.figureSize.width = 116;
%set panel height
plotDetails.figureSize.height = 175;
%Choose which experiment to plot, using the label in the metadata file
plotDetails.label = 'Stimulation_8';
%Set the labels for the x (time) and y (position) axes
plotDetails.xlabel = 'Horizontal position (�m)';
plotDetails.ylabel = 'Time (min)';
%Report the frame in which the electrode was inserted into the skin (don't show data before
%then)
plotDetails.firstFrame = 3;
%Choose the speed range for the colormap
plotDetails.displayVelocity = 4; %in �m/min
plotDetails.colorbarLabel = {'Horizontal velocity';
                             'component (�m/min)'};
plotDetails.cathodeMarkerColor = [30, 142, 224];

panelList(idx).plotDetails = plotDetails;

%% Fig. 4E: Velocity Different Phases
idx = idx + 1;
panelList(idx).panelName = '4E';

dataProcessingDetails = struct('dataFile',[],...
                               'metaDataFile',[]);
dataProcessingDetails.dataFile = '../data/20200803_stimulation_allVelocityXKymographs.mat';
dataProcessingDetails.metaDataFile = '../data/20200722_stimulation_DataRecord.csv';
%Exclude points lying too close to the electrodes when averaging velocity
dataProcessingDetails.pixelsAwayFromElectrode = 100;
%Exclude frames too close to when electrodes were switched in computing
%average velocity
dataProcessingDetails.framesAwayFromSwitch = 5;

panelList(idx).dataProcessingDetails = dataProcessingDetails;

plotDetails = struct('figureSize', struct('width',[],'height',[]),...
     ...%Choose which experiment to plot, by label (matching to the metadata)
        'label', [],...
        'xlabel',[],'ylabel',[]);
%set panel width
plotDetails.figureSize.width = 105;
%set panel height
plotDetails.figureSize.height = 110;
%set x and y axis limits
plotDetails.xlim = [-2.2, 2.2];
plotDetails.ylim = [0.8, 4.2];
%set x and y labels 
plotDetails.xlabel = {'Median horizontal'; 'velocity component'; '(�m/min)'};
%Choose which experiments (each from a single larva) to plot on the graph,
%by label
plotDetails.larvaeToPlot = {'Stimulation_6','Stimulation_7','Stimulation_8'};
%Choose the number of larvae to plot
plotDetails.nLarvae = numel(plotDetails.larvaeToPlot);
%Choose colors for cathode-anterior-first and cathode-posterior-first
%examples
plotDetails.colorCathodeAnteriorFirst = [178.5, 0, 178.5];
plotDetails.colorCathodePosteriorFirst = [0, 178.5, 0];
%Choose the text to display in the legend for each situation
plotDetails.legendCathodeAnteriorFirst = 'Ant. then Post.';
plotDetails.legendCathodePosteriorFirst = 'Post. then Ant.';
panelList(idx).plotDetails = plotDetails;

%% Fig. 4F : Stimulated vs unstimulated wounding
idx = idx + 1;
panelList(idx).panelName = '4F';

dataProcessingDetails = struct('dataFile',[],...
                               'metaDataFile',[]);
dataProcessingDetails.dataFile = '../data/20200804_stimWounding_allVelocityXKymographs.mat';
dataProcessingDetails.metaDataFile = '../data/20200804_stimWounding_DataRecord.csv';

dataProcessingDetails.rowsToAvg = 40; %each row represents 10�m
dataProcessingDetails.timepointsToShow = 20; %each timepoint is 30 seconds

panelList(idx).dataProcessingDetails = dataProcessingDetails;

%define structure to hold info about plotting this figure
plotDetails = struct(...
    'figureSize', struct('width',[], 'height', []),... %figure size in pts
    'xlabel',[],'ylabel',[],...
 ...%number of separate conditions to be plotted (computed from the list
    'nConditions', [],...
 ...%Plotting information for each condition: label to use to access the
 ...%data, color to plot the data, and the label to display in the legend
    'conditions', struct('label',[],'color',[],'displayLabel',[]),...
    'legendLineLength', [], 'legendTextLeftPosition',[]...
    );

%set panel width (in pts, 72pts/inch)
plotDetails.figureSize.width = 130;
%set panel height
plotDetails.figureSize.height = 132;
%Set x and y axis labels
plotDetails.xlabel = {'Horizontal velocity'; 'component (�m/min)'};
plotDetails.ylabel = {'Time post wounding'; '(min)'};
%Set the y axis limit
plotDetails.xlim = [-5.5, 5.5];
plotDetails.ylim = [0,10];
% Define the conditions used in the figure
plotDetails.conditions(1).label = 'none';
plotDetails.conditions(1).color = [0,0,0];
plotDetails.conditions(1).displayLabel = 'Unstimulated (N = 8)';

plotDetails.conditions(2).label = 'stimulation';
plotDetails.conditions(2).color = [25, 118, 187];
plotDetails.conditions(2).displayLabel = 'Stimulated (N = 4)';

%Number of conditions to plot determined from the conditions
plotDetails.nConditions = numel(plotDetails.conditions);
% Resize the position of the legend lines: start and stop position and the
% left position of the legend text for each legend label.
plotDetails.legendLineLength = [0.05 0.15];
plotDetails.legendTextLeftPosition = plotDetails.legendLineLength(2) + 0.05;

panelList(idx).plotDetails = plotDetails;
