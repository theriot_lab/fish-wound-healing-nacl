%% Fig 3B: Relative volume over time
panelName = '3B';

[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);
dataToPlot = processDataforVolumeOverTime(dataProcessingDetails, plotDetails);
%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%--------------------------------------
% Report the different number of fish and unique cell clusters
metaDataTable = readtable(dataProcessingDetails.metaDataFile);
nClusters = zeros(plotDetails.nConditions,1);
nFish = zeros(plotDetails.nConditions,1);
conditionList = cell(plotDetails.nConditions,1);
for k = 1:plotDetails.nConditions
    thisCondition = plotDetails.conditions(k).label;
    matchCondition = strcmp(metaDataTable.label, thisCondition);
    conditionList{k} = thisCondition;
    nClusters(k) = numel(unique(metaDataTable.datasetID(...
                           matchCondition)));
    uniqueFish = findgroups(metaDataTable.fishNumber(matchCondition),...
                            metaDataTable.dateAcquired(matchCondition));
    nFish(k) = max(uniqueFish);
end
numbersTable = table(conditionList,nClusters,nFish);
disp(numbersTable);

%% Fig 3C part 1: showing absolute differences
panelName = "3C1";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);
%load data
volumeData = readtable(dataProcessingDetails.volumeDataFile);

%------------------------
%Plot the paired data with lines
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on

for k = 1:plotDetails.nConditions
    thisConditionIDs = volumeData.datasetID(strcmp(volumeData.label,...
                                       plotDetails.conditions(k).label));
    thisConditionIDs = unique(thisConditionIDs);
    for m = 1:numel(thisConditionIDs)
        %Table has pre then post-wounding volume next to each other
        x = volumeData.volumeInUM3(volumeData.datasetID == thisConditionIDs(m));
        plot([2*k-1;2*k],x,...
             'color',(1/255)*plotDetails.conditions(k).color,'LineWidth',1)
    end
end

%--------------------------
%Calculate statistics for paired differences
statsTable = table('Size', [0, 8],...
              'VariableTypes', {'char','double','double', 'double', 'double','double','double','double'},...
              'VariableNames', {'condition', 'nCells','nLarvae', 'rejectNull', 'p','meanDiff','stdDiff','d'});
%Create a temporary struct holder for the info to add to statsTable
statsHolder = struct('condition', [], 'nCells', [], 'nLarvae', [], 'rejectNull', [], 'p', [], 'meanDiff',[],'stdDiff',[],'d',[]);
conditionPairedDifferencesList = cell(plotDetails.nConditions,1);
for k = 1:plotDetails.nConditions
    thisConditionIDs = volumeData.datasetID(strcmp(volumeData.label,...
                                       plotDetails.conditions(k).label));
    thisConditionIDs = unique(thisConditionIDs);
    %Find the part of the table that matches the IDs for this condition
    subsetData = volumeData(ismember(volumeData.datasetID, thisConditionIDs),:);
    %Group data by larvae to create a pooled average for each larvae
    larvaID = findgroups(subsetData.fishNumber,subsetData.dateAcquired);
    %Get the paired difference for each larvae in this condition
    conditionPairedDifferences = zeros(max(larvaID),1);
    for larva = 1:max(larvaID)
        dataThisGroup = subsetData(larvaID==larva,:);
        vPre = dataThisGroup.volumeInUM3(1:2:end);
        vPost = dataThisGroup.volumeInUM3(2:2:end);
        pairedDifferences = vPost - vPre;
        conditionPairedDifferences(larva) = mean(pairedDifferences);
    end
    conditionPairedDifferencesList{k} = conditionPairedDifferences;
    [h,p] = ttest(conditionPairedDifferences);
    statsHolder.condition = plotDetails.conditions(k).label;
    statsHolder.nCells = numel(thisConditionIDs);
    statsHolder.nLarvae = max(larvaID);
    statsHolder.rejectNull = h;
    statsHolder.p = p;
    statsHolder.meanDiff = mean(conditionPairedDifferences);
    statsHolder.stdDiff = std(conditionPairedDifferences);
    statsHolder.d = statsHolder.meanDiff ./ statsHolder.stdDiff;
    statsTable = [statsTable; struct2table(statsHolder)];
end
disp(statsTable)
%Compare explicitly to the unwounded case
condition = statsTable.condition;
unwoundedIdx = find(strcmp(condition,'noWound'));
rejectNull = zeros(plotDetails.nConditions,1);
p2 = zeros(plotDetails.nConditions,1);
for k = 1:plotDetails.nConditions
    [p, h] = ranksum(conditionPairedDifferencesList{k},...
                    conditionPairedDifferencesList{unwoundedIdx});
    rejectNull(k) = h;
    p2(k) = p;
end

statsTable2 = table(condition, rejectNull, p2);
disp(statsTable2)

%% Fig. 3C part 2: showing paired mean differences
panelName = "3C2";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);
%Load data
volumeData = readtable(dataProcessingDetails.volumeDataFile);

%-----------------------------
%Compute the paired difference confidence intervals and distributions

%Hold the confidence intervals (from bootstrapping)
ci_list = zeros(2,plotDetails.nConditions);
%Hold the ks-density functions (to plot the smoothed distributions)
fList = cell(plotDetails.nConditions,1);
%Hold the support for the density above (for plotting purposes)
xiList = cell(plotDetails.nConditions,1);
%To set axis limits, keep track of the minimum and maximum across all
%supports
min_xi = inf;
max_xi = -inf;
%Hold the means of each paired difference (for plotting notches)
meanList = zeros(plotDetails.nConditions,1);
for k = 1:plotDetails.nConditions
    useIdx = strcmp(plotDetails.conditions(k).label, volumeData.label);
    thisVolume = (1/1000) * volumeData.volumeInUM3(useIdx); %in picoliters
    %Volumes before and after wounding alternate in the table
    pairedDifferences = thisVolume(2:2:end) - thisVolume(1:2:end);
    meanList(k) = mean(pairedDifferences);
    [ci,samples] = bootci(10000,@mean,pairedDifferences);
    %Choose an empirical smoothing function for the distribution
    hopt = 1.06*std(samples)*numel(samples)^(-0.2);
    %Create a smooth kernel density on a support specified by L and U
    L = min(pairedDifferences);
    U = max(pairedDifferences);
    [f,xi] = ksdensity(samples,'Support',[L-10,U+10]);
    %Update the holder variables
    min_xi = min(min_xi, min(xi));
    max_xi = max(max_xi, max(xi));
    fList{k} = f;
    xiList{k} = xi;
    ci_list(:,k) = ci;
end
%-------------------------
%Plot the data (each distribution gets its own subplot)
fig = initializeFigure(panelName, [300,300], plotDetails);
figure(fig)

for k = 1:plotDetails.nConditions
    subplot(plotDetails.nConditions,1,k)
    hold on
    f = fList{k}/max(fList{k});
    xi = xiList{k};
    thisColor = (1/255)*plotDetails.conditions(k).color;
    %Plot the distribution (solid fill with transparency)
    fill( [xi, xi(1)],[f,f(1)],thisColor,'FaceAlpha',0.3,'EdgeColor','none');
    %Plot the confidence interval (solid line at the bottom)
    plot(ci_list(:,k),[0;0],'LineWidth',2,'Color',thisColor)
    %Plot the mean (notch in the solid line)
    plot([meanList(k),meanList(k)],[0,0.3], 'Color',thisColor,'LineWidth',2)
    %Plot the origin for common reference across all distributions
    plot([0,0],[0,1],'LineWidth',1,'Color','k')
    xlim([min_xi,0.8])
    if k == plotDetails.nConditions
        xlabel(plotDetails.xlabel)
    end
    ax = gca;
    ax.FontSize = 11;
    ax.XColor = [0,0,0];
    ax.YColor = [0,0,0];
    setTightMargins(ax);
end 

%% Figure 3D: Speed over time for many conditions
%Find figure 3D and get the parameters for this panel
panelName = "3D";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);

dataToPlot = processDataForSpeedOverTime(dataProcessingDetails, plotDetails);
%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%% Figure 3E: Volume over time for sodium chloride conditions
panelName = '3E';

[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);
dataToPlot = processDataforVolumeOverTime(dataProcessingDetails, plotDetails);
%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%--------------------------------------
% Report the different number of fish and unique cell clusters
metaDataTable = readtable(dataProcessingDetails.metaDataFile);
nClusters = zeros(plotDetails.nConditions,1);
nFish = zeros(plotDetails.nConditions,1);
conditionList = cell(plotDetails.nConditions,1);
for k = 1:plotDetails.nConditions
    thisCondition = plotDetails.conditions(k).label;
    matchCondition = strcmp(metaDataTable.label, thisCondition);
    conditionList{k} = thisCondition;
    nClusters(k) = numel(unique(metaDataTable.datasetID(...
                           matchCondition)));
    uniqueFish = findgroups(metaDataTable.fishNumber(matchCondition),...
                            metaDataTable.dateAcquired(matchCondition));
    nFish(k) = max(uniqueFish);
end
numbersTable = table(conditionList,nClusters,nFish);
disp(numbersTable);

%% Figure 3F: PC1 score from speed-over-time PCA vs initial relative volume change

panelName = "3F";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure3_parameters,...
                                                    panelName);

% Get the speed PCA for all conditions
load(dataProcessingDetails.speedDataFile, 'allKymographs', 'kymographTCoordinates');
metadata = readtable(dataProcessingDetails.speedMetadataFile);
speed = computeSpeedOverTimeForKymographList(allKymographs,...
          dataProcessingDetails.rowsToAvg);
% Restrict the speed to points so there are no NaNs (i.e. timepoints
% observed in all the samples)
speedForPCA = speed(:, 1:dataProcessingDetails.timepointsToShow);
score = runPCAOnData(speedForPCA);
speedLabelList = metadata.experimentalTreatmentPreWounding;

% Get the initial relative volume change
volumeData = readtable(dataProcessingDetails.volumeDataFile);
uniqueIdx = unique(volumeData.datasetID);
relativeVolumeChange = zeros(numel(uniqueIdx), 1);
for k = 1:numel(uniqueIdx)
    thisVolumeData = volumeData.volumeInUM3(volumeData.datasetID == uniqueIdx(k));
    relativeVolumeChange(k) = thisVolumeData(2) ./ thisVolumeData(1);
end

%Get the label for each entry in relativeVolumeChange (for averaging below
volumeLabelList = volumeData.label(1:2:end);

fig = initializeFigure(panelName, [1500,500], plotDetails);
figure(fig)
hold on
handleList = [];
NaClLabelList = {'NaCl135mM','NaCl100mM','NaCl068mM','none'};
%Holder for any data involving NaCl change (for running regression)
x = [];
y = [];
for k = 1:plotDetails.nConditions
    thisLabel = plotDetails.conditions(k).label;
    thisColor = (1/255)* plotDetails.conditions(k).color;
    thisSpeedIdx = strcmp(thisLabel, speedLabelList);
    thisVolumeIdx = strcmp(thisLabel, volumeLabelList);
    thisSpeedScore = score(thisSpeedIdx, 1);
    thisVolumeChange = relativeVolumeChange(thisVolumeIdx);
    
    %Get mean speed and initial volume change, get error bars from
    %bootstrapping
    meanSpeedScore = mean(thisSpeedScore);
    meanVolumeChange = mean(thisVolumeChange);
    speedCI = bootci(10000,@mean,thisSpeedScore) - meanSpeedScore;
    volumeCI = bootci(10000,@mean,thisVolumeChange) - meanVolumeChange;

    errorbar(meanVolumeChange,meanSpeedScore,-speedCI(1),speedCI(2),...
              -volumeCI(1),volumeCI(2),'Color',thisColor);
    %Collect the data that involves changing NaCl concentration (includes
    %hypo)
    if ismember(thisLabel, NaClLabelList)
        x = [x;meanVolumeChange];
        y = [y;meanSpeedScore];
    end
    h = plot(meanVolumeChange,meanSpeedScore,'.','MarkerSize',15,...
            'Color',thisColor);
    handleList = [handleList h];
end
%Do a least-squares regression on the NaCl datapoints to get a best-fit
%line
X = [ones(size(x)),x];
b = X\y;
xx = [1,1.5];
plot(xx,b(2)*xx+b(1),'k-','LineWidth',0.5)

%Get R^2 from correlation coefficient0
a = corrcoef(x,y);
R2 = a(1,2).^2
%Draw legend
legend(handleList,plotDetails.conditions.displayLabel)

ax = gca;
ax = basicFigureFormatting(ax, plotDetails);
