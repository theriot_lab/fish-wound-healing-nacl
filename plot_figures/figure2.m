%% Figure 2A: Speed over time for many conditions
%Find figure 2A and get the parameters for this panel
panelName = "2A";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);

dataToPlot = processDataForSpeedOverTime(dataProcessingDetails, plotDetails);
%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%{
%% Plot the normalized data for Fig. 1G
normalizedSpeed = dataToPlot(2).meanSpeed;
normalizedSpeed = (1/(max(normalizedSpeed) - min(normalizedSpeed))) * ...
                  (normalizedSpeed - min(normalizedSpeed));
tSpeed = dataToPlot(2).t;
save('20200505_NormalizedSpeedOverTime.mat','normalizedSpeed','tSpeed');
%}

%% Figure 2B: PCA on speed over time for many conditions
panelName = "2B";

% Load data and compute speed over time
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);
% load data
load(dataProcessingDetails.dataFile, 'allKymographs');
metadata = readtable(dataProcessingDetails.metadataFile);
speed = computeSpeedOverTimeForKymographList(allKymographs,...
          dataProcessingDetails.rowsToAvg);
% Restrict the speed to points so there are no NaNs (i.e. timepoints
% observed in all the samples).
% Run PCA on ALL the data, including some that will not be plotted here.
speedForPCA = speed(:, 1:dataProcessingDetails.timepointsToShow);
score = runPCAOnData(speedForPCA);
%Collect the scores from each condition in a cell array
dataArray = cell(plotDetails.nConditions, 1);
for k = 1:plotDetails.nConditions
    idxToUse = strcmp(metadata.experimentalTreatmentPreWounding,...
                  plotDetails.conditions(k).label);
    dataArray{k} = score(idxToUse,1);
end

% Plot the data
fig = initializeFigure(panelName, [500,100],plotDetails);
figure(fig)
hold on

%The plotSpread function requires a colormap in this form. Populate it
%while plotting the median lines
cMap = zeros(plotDetails.nConditions, 3);
for k = 1:plotDetails.nConditions
    plot([k-0.4, k+0.4], median(dataArray{k})*[1,1], 'LineWidth', 2,...
         'color', 0.5*[1,1,1])
    cMap(k,:) = (1/255)*plotDetails.conditions(k).color;
end
%Plot the individual scores using plotSpread
ah = plotSpread(dataArray, 'distributionColors', cMap, 'spreadFcn', {'lin',1},...
                'binWidth', 2, 'spreadWidth', 0.8);
%Increase the marker size
for k = 1:plotDetails.nConditions
    ah{3}.Children(k).MarkerSize = 15;
end
%---------------
%make it pretty
ax = gca;
ax = basicFigureFormatting(ax, plotDetails);
% Format ticks, including text on the horizontal axis
yticks(-10:5:15)
ax.TickLength = [0.02,0.025];
xticklabels({plotDetails.conditions.displayLabel});
ax.XTickLabelRotation = 25;
%Make the figure boundaries tight around the figure
ax = setTightMargins(ax);

%-------------------------
% Print statistics for 2B
%-------------------------
% Approach: Welch's 1-way anova followed by Games-Howell post-hoc tests
disp('Statistics for Figure 2B:')
%Get a vector of groups for the different conditions
G = zeros(size(score,1),1);
for k = 1:plotDetails.nConditions
    thisCondition = strcmp(metadata.experimentalTreatmentPreWounding,...
                           plotDetails.conditions(k).label);
    G(thisCondition) = k;
end
%Remove scores for datapoints that didn't match any of the tested
%conditions
scoresForStats = score(G~=0,1); 
GforStats = G(G~=0);
%Create a list of the different conditions that matches the group vector
groupNames = {plotDetails.conditions.label};
groupNamesForStats = groupNames(GforStats);
%Run the Welch's ANOVA
[p, F, df1, df2] = wanova(scoresForStats,GforStats);
disp("stats for Welch's ANOVA:")
s = sprintf('p: %d, F: %d, df1: %d, df2: %d',p,F,df1,df2);
disp(s);
%Run the Games-Howell post-hoc tests
disp("Hypothesis-tests for games-howell post-hoc")
[h,p,stats] = games_howell(scoresForStats,groupNamesForStats);
hAsTable = array2table(p,'RowNames',stats.gnames,'VariableNames',stats.gnames);
disp(hAsTable);


%% Figure 2E: LifeAct intensity over time following non-rigid registration
panelName = "2E";

[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);
%Get intensity data (slightly different function for intensity vs. speed)
dataToPlot = processDataForIntensityOverTime(dataProcessingDetails, plotDetails);
fig = initializeFigure(panelName, [100,500],plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%% Figure 2F: sodium concentration in two-chamber device
panelName = "2F";

[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);
fig = initializeFigure(panelName, [500,100],plotDetails);
figure(fig)
subplot('Position',plotDetails.imageSubplotPosition)
%Load image to be calibrated and calibration table
load(dataProcessingDetails.imageFile,'panorama_baseline');
calibrationTable = readtable(dataProcessingDetails.concentrationCalibration);
%Nonlinear regression on calibration data to fit a binding model for CoroNa
%Green intensity as a function of sodium concentration
bindingModel = fitnlm(calibrationTable, dataProcessingDetails.bindingCurve,...
                      dataProcessingDetails.initialGuess);
bindingCoefficientsFit = bindingModel.Coefficients.Estimate;

%To convert the pixel intensities into sodium concentration, apply the
%inverse binding curve with the fit parameters from above
panoramaCalibrated = dataProcessingDetails.inverseBindingCurve(bindingCoefficientsFit,...
                                                            panorama_baseline);
%Flip the image to match the A-P orientation of the rest of the paper
panoramaCalibrated = fliplr(panoramaCalibrated);
%Crop the image to emphasize the fish
panoramaCalibrated = panoramaCalibrated(1+plotDetails.cropRows:end-plotDetails.cropRows, :);
%Display image
imagesc(panoramaCalibrated,plotDetails.concentrationDisplayRange)
%Make it pretty
ax1 = gca;
ax1.XTickLabels = [];
ax1.YTickLabels = [];
%Add a colorbar
set(ax1,'Colormap',viridis)
cc = colorbar('westoutside');
ccTitleHandle = get(cc,'Title');
set(ccTitleHandle,'String',plotDetails.colorbarLabel,'FontSize',10);

%Resize the image appropriately, accounting for the colorbar
ax1.Position = plotDetails.imageSubplotPosition;
daspect([1 1 1])

%------------------------------
%Plot a linescan of concentration below the image
subplot('Position',plotDetails.linescanSubplotPosition)
%Load the linescan (made in ImageJ)
linescan = readtable(dataProcessingDetails.concentrationLineScan);
plot(linescan.X, linescan.Y, 'k-','LineWidth',2);
ax2 = gca;
ax2 = basicFigureFormatting(ax2, plotDetails);
xlim([0, max(linescan.X)])

%% Figure 2G: Relative velocity over time for two-chamber experiment
panelName = "2G";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);

dataToPlot = processDataForSpeedOverTime(dataProcessingDetails, plotDetails,...
    'measureRelative',true);
%Plot The Data
fig = initializeFigure(panelName, [500,100],plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%% Figure S2F: Speed over time for alpha bungarotoxin
panelName = "S2F";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);

dataToPlot = processDataForSpeedOverTime(dataProcessingDetails, plotDetails);
%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,800], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);

%% Figure S2A-D: PCA on speed over time for many conditions
panelName = "S2A-D";

%Load data and compute speed over time
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure2_parameters,...
                                                    panelName);
                                                
load(dataProcessingDetails.dataFile, 'allKymographs');
metadata = readtable(dataProcessingDetails.metadataFile);
speed = computeSpeedOverTimeForKymographList(allKymographs,...
          dataProcessingDetails.rowsToAvg);
% Restrict the speed to points so there are no NaNs (i.e. timepoints
% observed in all the samples)
speedForPCA = speed(:, 1:dataProcessingDetails.timepointsToShow);
[score, coeff, latent] = runPCAOnData(speedForPCA);
meanSpeed = mean(speedForPCA, 1);

%Initialize the figure
fig = initializeFigure(panelName, [800,800], plotDetails);
figure(fig)

%--------------------------
%Plot variation in the first principal component

subplot('Position',plotDetails.subplotPosition(1,:))
hold on
score_std = std(score,0,1);
t = 0.5*(0:dataProcessingDetails.timepointsToShow-1);
%Plot the mean profile +/- one standard deviation in score along PC1
plot(t,meanSpeed,'k-')
plot(t,meanSpeed + score_std(1) * coeff(:,1)','r-');
plot(t,meanSpeed - score_std(1) * coeff(:,1)','b-');

title(plotDetails.subplotTitle_1)
[~, icons, ~, ~] = legend('Mean','Mean + \sigma','Mean - \sigma');
ax = gca;
ax = basicFigureFormatting(ax, plotDetails, 1);
shortenLegendLines(icons, plotDetails);

%------------------------
%Plot variation in the 2nd principal component

subplot('Position',plotDetails.subplotPosition(2,:))
hold on

plot(t,meanSpeed,'k-')
plot(t,meanSpeed + score_std(2) * coeff(:,2)','r-');
plot(t,meanSpeed - score_std(2) * coeff(:,2)','b-');

title(plotDetails.subplotTitle_2)
[~, icons, ~, ~] = legend('Mean','Mean + \sigma','Mean - \sigma');
ax = gca;
ax = basicFigureFormatting(ax, plotDetails, 2);
shortenLegendLines(icons, plotDetails);

%------------------------
%Plot variance explained by each PC
subplot('Position', plotDetails.subplotPosition(3,:))
hold on

plot(100*(latent/sum(latent)),'o-','color','k','LineWidth',0.75)

ax = gca;
ax = basicFigureFormatting(ax, plotDetails, 3);

%-------------------------
%Scatter plot of PC1 vs PC2 scores
subplot('Position', plotDetails.subplotPosition(4,:))
hold on

legList = {};
for k = 1:plotDetails.nConditions
    idxToUse = strcmp(metadata.experimentalTreatmentPreWounding,...
                  plotDetails.conditions(k).label);
    plot(score(idxToUse,1),score(idxToUse,2),'.','MarkerSize',10,...
         'Color',(1/255)*plotDetails.conditions(k).color);
    legList = [legList plotDetails.conditions(k).displayLabel];
end

ax = gca;
ax = basicFigureFormatting(ax, plotDetails, 4);
leg = legend(legList);
legPos = leg.Position;
leg.Position = [plotDetails.legend4XY, legPos(3:4)];
axis equal
ax = setTightMargins(ax);
