%% Fig. 1E: Plot averaged speed kymograph and marginal displacements in each direction
panelName = "1E";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure1_parameters,...
                                                    panelName);

fig = initializeFigure(panelName, [200, 500], plotDetails);
figure(fig)
hold on
%Load data
load(dataProcessingDetails.dataFile, 'allKymographs');
metadata = readtable(dataProcessingDetails.metadataFile);
%Get just the kymographs from the condition being plotted
useList = find(strcmp(metadata.experimentalTreatmentPreWounding,plotDetails.condition));
%Create a dummy container for kymograph data (will be averaged over the 3rd
%dimension later
kymographContainer = nan(dataProcessingDetails.rowsToAvg,...
                         dataProcessingDetails.timepointsToShow,...
                         numel(useList));
%Fill the container, using at most the specified number of rows and columns
%from each kymograph
for k = 1:numel(useList)
    kymograph = allKymographs{useList(k)};
    r = min(dataProcessingDetails.rowsToAvg, size(kymograph, 1));
    c = min(dataProcessingDetails.timepointsToShow, size(kymograph, 2));
    kymographContainer(1:r,1:c,k) = kymograph(1:r,1:c);
end
averagedKymograph = nanmean(kymographContainer,3);

%-----------------------------------
%Plot the averaged kymograph
subplotPosition = plotDetails.subplotPosition_1;
subplot('Position',subplotPosition)
if isnan(plotDetails.colorbarRange)
    colorbarRange = [0, max(averagedKymograph(:))];
else
    colorbarRange = plotDetails.colorbarRange;
end
imagesc([0,20],[0,400],averagedKymograph, colorbarRange);
colormap(gca,magma)

%Make it pretty
ax = gca;
ax.YDir = 'normal'; %IMPORTANT - flips the y-axis of the kymograph
ax.FontSize = 11;
ax.XColor = [0 0 0];
ax.YColor = [0 0 0];
ylabel(plotDetails.ylabel_1);
ax.XTickLabels = [];
ax.TickDir = 'out';

%Add a colorbar
cc = colorbar('northoutside');
ccTitleHandle = get(cc,'Title');
set(ccTitleHandle,'String',plotDetails.colorbarLabel,'FontSize',12);

%Resize the kymograph so that it is a square (push the colorbar up)
p = ax.Position;
ax.Position = [p(1),subplotPosition(2:4)];

%-----------------------------------
%Plot the marginals for y axis (net displacements at different positions)
subplot('Position',plotDetails.subplotPosition_2)
hold on
for k = 1:numel(useList)
    kymograph = allKymographs{useList(k)};
    %convert speed (�m/min) to displacement (multiply by 0.5 min per frame)
    %and sum up the displacement over the first 20 minutes (over columns)
    d = 0.5*sum(kymograph(:,1:40),2);
    x = 10*(0:numel(d)-1);
    %Plot sideways to align with the kymograph
    plot(d(1:41),x(1:41),'k-','LineWidth',1)
end

%Make it pretty
ax = gca;
ax.FontSize = 11;
ax.XColor = [0 0 0];
ax.YColor = [0 0 0];
xlabel(plotDetails.xlabel_2);
ax.YTickLabel = [];

%-----------------------------------
%Plot the marginals for the x axis (net displacements over time)
subplot('Position',plotDetails.subplotPosition_3)
hold on
for k = 1:numel(useList)
    kymograph = allKymographs{useList(k)};
    %Convert speed to displacement as above, then sum up the net
    %displacement in the 400�m near the wound over time (sum along rows)
    d = 0.5 * sum(kymograph(:,1:41),1);
    t = 0.5*(0:numel(d)-1); %Timepoints are every 30 seconds i.e. 0.5 min
    plot(t,d,'k-','LineWidth',1);
end

%Make it pretty
ax = gca;
ax.FontSize = 11;
ax.XColor = [0 0 0];
ax.YColor = [0 0 0];
xlabel(plotDetails.xlabel_3);
ylabel(plotDetails.ylabel_3);

%% Fig. 1G: GCaMP kymograph
panelName = "1G";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure1_parameters,...
                                                    panelName);

load(dataProcessingDetails.dataFile);
fig = initializeFigure(panelName, [100,500], plotDetails);
figure(fig)
hold on
%Load data
load(dataProcessingDetails.dataFile, 'gcampKymographList',...
     'nlsKymographList','initialFrameList');
nMovies = numel(gcampKymographList);
%Create a dummy container for kymograph data (will be averaged over the 3rd
%dimension later
delFF0Container = nan(dataProcessingDetails.rowsToAvg,...
                         dataProcessingDetails.timepointsToShow,...
                         nMovies);

for k = 1:nMovies
    gcampKymograph = gcampKymographList{k};
    nlsKymograph = nlsKymographList{k};
    initialFrame = initialFrameList(k);
    %Normalize GCaMP intensity by nls intensity in each bin to correct for
    %differences in expression of the GCaMP-P2A-nls construct
    normalizedKymograph = gcampKymograph ./ nlsKymograph;
    %rows are spatial position, columns are timepoints
    [sizeX, sizeT] = size(normalizedKymograph);
    %Compute ?F/F0 relative to the timepoint just before wounding
    F0 = repmat(normalizedKymograph(:, initialFrame - 1),...
                1, sizeT - initialFrame +1);
    delFF0 = (normalizedKymograph(:, initialFrame:end) - F0) ./ F0;
    r = min(dataProcessingDetails.rowsToAvg, sizeX);
    c = min(dataProcessingDetails.timepointsToShow, sizeT);
    delFF0Container(1:r,1:c,k) = delFF0(1:r,1:c);
end
delFF0Average = nanmean(delFF0Container,3);

if isnan(plotDetails.colorbarRange)
    colorbarRange = [0, max(delFF0Average(:))];
else
    colorbarRange = plotDetails.colorbarRange;
end
imagesc([0,20],[0,400],delFF0Average, colorbarRange);
colormap(gca,magma)

%Make it pretty
ax = gca;
ax = basicFigureFormatting(ax,plotDetails);
ax.YDir = 'normal'; %IMPORTANT - flips the y-axis of the kymograph
ax.TickDir = 'out';

%Add a colorbar
cc = colorbar('northoutside');
ccTitleHandle = get(cc,'Title');
set(ccTitleHandle,'String',plotDetails.colorbarLabel,'FontSize',12);

%Resize the kymograph so that it is a square (push the colorbar up)
p = ax.Position;
ax.Position = [p(1:2),p(4),p(4)];

%% Fig. 1H: Plot normalized tracks of GCaMP and tissue speed over time

panelName = "1H";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure1_parameters,...
                                                    panelName);

load(dataProcessingDetails.dataFile);
fig = initializeFigure(panelName, [500,500], plotDetails);
figure(fig)
hold on
for k = 1:plotDetails.nConditions
    t = eval(plotDetails.conditions(k).tVecName); %Evaluates to the name of the variable
    if any(t > dataProcessingDetails.minToShow)
        maxIndex = find(t>=dataProcessingDetails.minToShow, 1, 'first');
    else
        maxIndex = numel(t);
    end
    y = eval(plotDetails.conditions(k).normalizedTrackName);
    plot(t(1:maxIndex), y(1:maxIndex), 'LineWidth', 2,...
         'color', (1/255)*plotDetails.conditions(k).color);
end
ax = gca;
basicFigureFormatting(ax,plotDetails);
legend({plotDetails.conditions.displayLabel});

%% Fig S1B: tail transection vs laceration

panelName = "S1B";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure1_parameters,...
                                                    panelName);

%Load the data and process it: get averages and bootstrapped confidence
%intervals from tracks from individual larvae
dataToPlot = processDataForSpeedOverTime(dataProcessingDetails, plotDetails);

%Plot The Data
%Set the desired size of the panel in pts (~72 pts/inch)
fig = initializeFigure(panelName, [100,100], plotDetails);
figure(fig)
hold on
ax = gca;
ax = linePlotErrorBars(ax, dataToPlot, plotDetails);


%% Fig. S1C: examples of tracking
panelName = "S1C";
[dataProcessingDetails,...
             plotDetails] = getDetailsFromPanelName(@figure1_parameters,...
                                                    panelName);
%Load data
reader = bfGetReader(dataProcessingDetails.imageFile);
load(dataProcessingDetails.coordinateFile,'coordinateArray','marginCoordinateArray');

sizeT = reader.getSizeT();
im = bf_getFrame(reader, 1, 1, 1);

fig = initializeFigure(panelName, [700,100],plotDetails);
figure(fig)
hold on

%--------------------------------------
% Plot some initial points
subplot('Position',plotDetails.subplotPosition_1)
hold on

%Initialize the random seed
rng(dataProcessingDetails.rSeed);

%Only consider points that existed in the first frame (and persist to the
%second)
allCoordinates = [coordinateArray; marginCoordinateArray];
valid = ~any(isnan(allCoordinates(:,1:2)),2);
useCoordinates = allCoordinates(valid,1:2);

%Only show a fraction of the points in the first frame
nPoints = round(dataProcessingDetails.fracInitialPoints ...
             * size(useCoordinates,1));
pointIndex = randi(size(useCoordinates,1), nPoints, 1);
%Display the first image
imshow(im, [], 'InitialMagnification', 75);
%Display the initial locations of this subset of coordinates
plot(useCoordinates(pointIndex, 1), useCoordinates(pointIndex, 2), ...
     '.', 'MarkerSize', 7, 'color', 'y');
%--------------------------------------
% Plot a few points over time
subplot('Position',plotDetails.subplotPosition_2)
hold on

%Initialize the random seed
rng(dataProcessingDetails.rSeed);
%Choose a small number of points near the wound, where "nearby" is chosen
%arbitrarily for this particular movie based on the x-coordinate of the
%point
allValid = ~any(isnan(coordinateArray),2);
useCoordinates = coordinateArray(allValid,:);
nPoints = dataProcessingDetails.nTracks;
nearPoints = find(useCoordinates(:,1) > dataProcessingDetails.nearPointThreshold);
randomSelection = randi(numel(nearPoints), nPoints, 1);
pointIndex = nearPoints(randomSelection);
%Because more movement at the beginning than the end, logarithmically scale
%the colormap to visually appear more even
cmap = cool(ceil(10*log(sizeT))+1);
%Show the first frame of the movie
imshow(im, [], 'InitialMagnification', 50);
%Plot out the tracks as a series of lines, changing color with time.
for k = 1:numel(pointIndex)
    for t = 1:(sizeT-1)
        plot(useCoordinates(pointIndex(k),(2*t-1):2:(2*t+1)),...
             useCoordinates(pointIndex(k),2*t:2:(2*(t+1))),...
             'LineWidth',2,'Color',cmap(ceil(10*log(t))+1,:));
    end
end
%Plot the initial and final locations of each track with different colors
plot(useCoordinates(pointIndex,1),useCoordinates(pointIndex,2),'o','MarkerSize',8,'Color',cmap(1,:));
plot(useCoordinates(pointIndex,end-1),useCoordinates(pointIndex,end),'o','MarkerSize',8,'Color',cmap(end,:));

subplot('Position',plotDetails.subplotPosition_5)
colormap(gca,cmap)
colorbar('southoutside')
set(gca,'Visible','off')

%-----------------------------
% Color-code the points by the total distance they were tracked
subplot('Position',plotDetails.subplotPosition_3)
hold on

%Initialize the random seed
rng(dataProcessingDetails.rSeed);

%Consider points that were tracked through the entire movie
allValid = ~any(isnan(allCoordinates),2);
useCoordinates = allCoordinates(allValid,:);
%Only look at a subset of points for display purposes
nPoints = round(dataProcessingDetails.fracTrackedPoints ...
             * size(useCoordinates, 1));
pointIndex = randi(size(useCoordinates, 1), nPoints, 1);
useCoordinates = useCoordinates(pointIndex,:);

%Compute the total distance each point was tracked
u = (useCoordinates(:, 3:2:end) - useCoordinates(:, 1:2:end-2)).^2;
v = (useCoordinates(:, 4:2:end) - useCoordinates(:, 2:2:end-2)).^2;
micrometers = ome.units.UNITS.MICROM; %This will fail if bioformats_package.jar is not on your javaclasspath
omeMeta = reader.getMetadataStore();
pixelSizeInMicrons = omeMeta.getPixelsPhysicalSizeX(0).value(micrometers).doubleValue();
totalDistance = pixelSizeInMicrons * sum(sqrt(u + v), 2);
%Bin the distance into 256 bins
[~,~,binIndex] = histcounts(totalDistance, 256);
cmap = plasma(256);
%Show the first frame
imshow(im, [], 'InitialMagnification', 50);
for k = 1:256 %Loop through bins
    %Select the points falling in this particular bin
    ii = binIndex == k;
    plot(useCoordinates(ii, 1), useCoordinates(ii, 2), '.', ...
         'MarkerSize', 7, 'color', cmap(k,:));
end

subplot('Position',plotDetails.subplotPosition_6)
colormap(gca,plasma)
colorbar('southoutside')
set(gca,'Visible','off')

